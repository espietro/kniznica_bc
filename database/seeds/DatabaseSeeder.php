<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//         $this->call(UsersTableSeeder::class);

        DB::unprepared('
        
        INSERT into	users (name,email,password, phone, institution)
	VALUES
	("Peter", "p@p.com", "$2y$10$AjgyJRjCgZVMCaxGwbrlZ.gkNlVuwHtOZxh1Cu8pcYl.0rJ7EQCpi", "+421901222333", "ZS Zilina"),
	("Jano", "jano@jano.com", "$2y$10$AjgyJRjCgZVMCaxGwbrlZ.gkNlVuwHtOZxh1Cu8pcYl.0rJ7EQCpi", "+421901222333", "MS Zilina"),
	("Danka", "danka@danka.com", "$2y$10$AjgyJRjCgZVMCaxGwbrlZ.gkNlVuwHtOZxh1Cu8pcYl.0rJ7EQCpi", "+421901222333", "ZS Zilina"),
	("D", "d@d.com", "$2y$10$AjgyJRjCgZVMCaxGwbrlZ.gkNlVuwHtOZxh1Cu8pcYl.0rJ7EQCpi", "+421901222333", "ZS Zilina");


INSERT into	events (title,duration,subject, created_at, updated_at)
	VALUES("Deduško večerníček",60,"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", now(), now()),
	("Danka a Janka - Mária Ďuríčková",60,"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", now(), now()),
	("Ako sa snežienky takmer zbláznili-Peter Stoličný",60,"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", now(), now());

INSERT into	events ( visible,title,duration,subject, created_at, updated_at)
	VALUES(FALSE, "Neviditelny event",60,"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", now(), now());


INSERT INTO event_user VALUES
(2,3,TRUE), (3,1,TRUE),
(1,1,TRUE),(1,3,FALSE),(4,3,TRUE);
        
        INSERT INTO tgroups (name)
	VALUES ("Prvy stupen"), ("Druhy stupen"), ("MS");


INSERT INTO event_tgroup (event_id, tgroup_id) 
	VALUES (1, 1),
	(2,2),
	(2,1),
	(3,2);
        
        INSERT INTO roles (name)
	VALUES ("admin"), ("user"), ("employee");

INSERT INTO role_user (role_id,user_id)
	VALUES (1,1),(2,2),(3,3);
	
	INSERT INTO places (name) 
VALUES ("A"), ("B"), ("BLOK");

UPDATE places
SET id = 0
where name = "BLOK";

INSERT INTO event_place 
VALUES (1,1), (1,2), (2,2), (3,1);

INSERT INTO test.opening_hrs (id,start_time,end_time)
VALUES (0,\'00:00:00\',\'00:00:00\'),
(1,\'07:00:00\',\'18:00:00\'),
 (2,\'07:00:00\',\'18:00:00\'),
 (3,\'11:00:00\',\'19:00:00\'),
 (4,\'07:00:00\',\'18:00:00\'),
 (5,\'07:00:00\',\'18:00:00\'),
 (6,\'09:00:00\',\'13:00:00\');
        ');


        factory(App\Availability::class, 10)->create();
        factory(App\Occurrence::class, 10)->create();
    }


    /*

    =========> pre phpmyadmin <==========

INSERT INTO zaknizapp1.opening_hrs (id,start_time,end_time)
VALUES (0,"00:00:00","00:00:00"),
(1,"07:00:00",'18:00:00'),
 (2,'07:00:00','18:00:00'),
 (3,'11:00:00','19:00:00'),
 (4,'07:00:00','18:00:00'),
 (5,'07:00:00','18:00:00'),
 (6,'09:00:00','13:00:00');

 INSERT INTO roles (name)
	VALUES ("admin"), ("user"), ("employee");

    INSERT INTO places (id, name)
VALUES (0, "BLOK");



    */

}
