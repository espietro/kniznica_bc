<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOccurrencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('occurrences', function (Blueprint $table) {
             $table->increments('id');

            $table->integer('event_id')->unsigned();
            $table->integer('organiser_id')->unsigned();
            $table->integer('place_id')->unsigned()->nullable()->comment("where this event takes place");
            $table->integer('user_id')->unsigned()->nullable()->comment("attendee");
            $table->dateTime('start_time')->comment("when will event happen");
            $table->dateTime('end_time');
            $table->boolean('confirmed')->default(false);

            $table->timestamps();
        });

        Schema::table('occurrences', function (Blueprint $table) {
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
            $table->foreign('organiser_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('place_id')->references('id')->on('places');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('occurrences', function (Blueprint $table) {
            $table->dropForeign(['event_id']);
            $table->dropForeign(['organiser_id']);
            $table->dropForeign(['place_id']);
            $table->dropForeign(['user_id']);
        });

        Schema::dropIfExists('occurrences');
    }
}
