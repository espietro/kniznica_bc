<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTgroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_tgroup', function (Blueprint $table) {
            $table->integer('event_id')->unsigned();
            $table->integer('tgroup_id')->unsigned();

            $table->primary(['event_id','tgroup_id']);
        });

        Schema::table('event_tgroup', function (Blueprint $table) {
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
            $table->foreign('tgroup_id')->references('id')->on('tgroups')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('event_tgroup', function (Blueprint $table) {
            // $table->dropForeign(['event_id']);
            // $table->dropForeign(['tgroup_id']);
            
            // $table->dropPrimary(['event_id']);
            // $table->dropPrimary(['tgroup_id']);
        });
        
        Schema::dropIfExists('event_tgroup');
    }
}
