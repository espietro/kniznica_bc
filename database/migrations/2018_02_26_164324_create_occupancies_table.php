<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOccupanciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('occupancies', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('start_time');
            $table->dateTime('end_time');
            $table->integer('place_id')->unsigned();
            $table->integer('occurrence_id')->unsigned()->nullable();
            $table->timestamps();
        });

        Schema::table('occupancies', function (Blueprint $table) {
            $table->foreign('place_id')->references('id')->on('places')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('occupancies', function (Blueprint $table) {
            $table->dropForeign(['place_id']);
        });

        Schema::dropIfExists('occupancies');
    }
}
