<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        CREATE TRIGGER insert_on_occurrency
        AFTER INSERT ON occurrences 
        FOR EACH ROW
        BEGIN
            INSERT INTO occupancies (place_id,start_time,end_time,occurrence_id,created_at, updated_at)
                VALUES (NEW.place_id, NEW.start_time, NEW.end_time, NEW.id, NEW.created_at, NEW.updated_at); 
        END
        ');

        DB::unprepared('
        CREATE TRIGGER delete_on_occurrency
        BEFORE DELETE ON occurrences 
        FOR EACH ROW
        BEGIN
            DELETE FROM occupancies
             WHERE (occurrence_id = OLD.id); 
        END
        ');

        DB::unprepared('
        CREATE TRIGGER update_on_occurrency
        AFTER UPDATE ON occurrences 
        FOR EACH ROW
        BEGIN
            UPDATE occupancies 
            SET start_time = NEW.start_time, end_time = NEW.end_time, updated_at = NEW.updated_at
            WHERE (occurrence_id = id); 
        END;
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER IF EXISTS insert_on_occurrency');
        DB::unprepared('DROP TRIGGER IF EXISTS delete_on_occurrency');
        DB::unprepared('DROP TRIGGER IF EXISTS update_on_occurrency');
    }
}
