<?php

use Faker\Generator as Faker;
use \Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

//$factory->define(App\User::class, function (Faker $faker) {
//    static $password;
//
//    return [
//        'name' => $faker->name,
//        'email' => $faker->unique()->safeEmail,
//        'password' => $password ?: $password = bcrypt('secret'),
//        'remember_token' => str_random(10),
//    ];
//});

//$factory->define(App\Availability::class, function (Faker $faker) {
//
//    $start_time = $faker->dateTimeBetween('2018-03-05 07:00:00','2018-03-05 18:00:00');
//
//    $ret=[
//        'start_time' => $faker->dateTimeBetween(Carbon::createFromFormat( "Y-m-d H:i:s",'2018-03-05 07:00:00'),
//                                                Carbon::createFromFormat( "Y-m-d H:i:s",'2018-03-05 17:00:00')
//        ),
//        'end_time' => $faker->dateTimeBetween($start_time,Carbon::createFromFormat( "Y-m-d H:i:s",'2018-03-05 18:00:00')),
//        'user_id' => $faker->numberBetween(1,3)
//    ];
//
//    return $ret;
//});

$factory->define(App\Availability::class, function (Faker $faker)
{

    while (true)
    {
        $date = Carbon::tomorrow()->addDays(rand(1, 7));
        if ($date->dayOfWeek != 0)
        {
            break;
        }
    }


    $workHrs = \DB::table('opening_hrs')->find($date->dayOfWeek);

    $multiply = rand(0, 1);

    $workDayStart = $date->copy()->setTimeFromTimeString($workHrs->start_time)->addHours($multiply * 6);
    $date = $date->setTimeFromTimeString($workHrs->start_time);


    if ($date->copy()->setTimeFromTimeString($workHrs->start_time)->eq($date->copy()->setTimeFromTimeString($workHrs->end_time)))
    {
        $workDayStart = $date->copy();
    }

    $workDayEnd = $workDayStart->copy()->addHours(6);

    $date = $date->setTimeFromTimeString($workHrs->end_time);

    if ($workDayEnd->gt($date))
    {
        $workDayEnd = $date;
    }

    if ($workDayStart->gt($workDayEnd))
    {
        $workDayStart->setTimeFromTimeString($workHrs->start_time);
    }


    return [
        'start_time' => $workDayStart,
        'end_time' => $workDayEnd,
        'user_id' => rand(0, 1) ? 1 : 3
    ];
});

$factory->define(App\Occurrence::class, function (Faker $faker)
{

    $availStart = null;
    $availEnd = null;
    while (true)
    {
        $availability = \App\Availability::all()->random();

        //get day of start up to 7 days from tomorow
        $availStart = Carbon::parse($availability->start_time);
        $availEnd = Carbon::parse($availability->end_time);

        if ($availStart->eq($availEnd))
        {
            $availability = \DB::table('availabilities')->find(rand(1, \App\Availability::all()->count()));
            continue;
        }
        break;
    }

    $timeOfStart = $faker->dateTimeBetween($availStart, $availEnd);

    $timeOfStart = Carbon::parse($timeOfStart->format('Y-m-d H:i:s'))->second(0);

    while (true)
    {
        $event = \App\Event::all()->random();
        if ($event->visible)
        {
            break;
        }
    }

    //get only users that can organise this event
    $organiserId = $event->users->random()->id;


    if ($timeOfStart->copy()->addMinutes($event->duration)->gt($availEnd))
    {
        $timeOfStart = $availEnd->copy()->subMinutes($event->duration);
    }

    $timeOfEnd = $faker->dateTimeBetween($timeOfStart->copy()->addMinutes($event->duration), $availEnd);


    $placeId = $event->places->random();

//    \DB::table('occupancies')->insert([
//       ['start_time'=> $timeOfStart, 'end_time'=>$timeOfEnd, 'place_id' => $placeId]
//    ]);


    return [
        'event_id' => $event->id,
        'organiser_id' => $organiserId,
        'place_id' => $placeId,
        'user_id' => 2,
        'start_time' => $timeOfStart,
        'end_time' => $timeOfEnd,
        'confirmed' => rand(0, 1)
    ];
});


