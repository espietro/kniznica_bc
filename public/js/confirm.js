
$(document).ready(
    function () {
    $(".delete").click( function (event) {
        var answer = confirm("Naozaj chcete odstrániť tento účet?" +
            "\npozn.: Zatiaľ nebudú odstránené podujatia a udalosti, s ktorými je tento používateľ spojený." +
            "\nOdstránený bude iba účet. " +
            "\n(V konečnej verzii sa spolu s ním odstránia aj podujatia a udalosti. Ak s tým nesúhlasíte kontaktujte ma mailom.)");

        if (!answer)
        {
            event.preventDefault();
        }
    });
});