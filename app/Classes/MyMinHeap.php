<?php
/**
 * Created by PhpStorm.
 * User: peter
 * Date: 6.3.2018
 * Time: 14:47
 */

namespace App\Classes;

use Carbon\Carbon;

class MyMinHeap extends \SplMinHeap
{

    //determines if intervals should be also compared by its eventId, suitable when reservation with target group is chosen
    private $noEvent;

    public function __construct($noEvent = false)
    {
        $this->noEvent = $noEvent;
    }


    /**
     * Result of the comparison, positive integer if value1 is lower than value2, 0 if they are equal, negative integer otherwise.
     * @param $value1 AvailableInterval::class
     * @param $value2 AvailableInterval::class
     * @return int
     */
    protected function compare($value1, $value2)
    {
        $time1 = Carbon::parse($value1->startTime);
        $time2 = Carbon::parse($value2->startTime);

        if (!$this->noEvent)
        {
            if ($time1->lt($time2))
            {
                return 1;
            }
            elseif ($time1->eq($time2))
            {
                return 0;
            }
            return -1;
        }
        else
        {
            if ($value1->eventId == $value2->eventId)
            {
                if ($time1->lt($time2))
                {
                    return 1;
                }
                else if ($time1->eq($time2))
                {
                    return 0;
                }
                return -1;
            }
            elseif ($value1->eventId < $value2->eventId)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }
    }




}