<?php
/**
 * Created by PhpStorm.
 * User: peter
 * Date: 13.3.2018
 * Time: 16:16
 */

namespace App\Classes;


use App\Place;

/*
 * Class only works as factory for calendar.
 */
class Calendar
{
    private static $months = ['Január', 'Február', 'Marec', 'Apríl', 'Máj', 'Jún', 'Júl', 'August', 'September', 'Október', 'November', 'December'];
    private static $days = ['Ne', 'Po', 'Ut', 'St', 'Št', 'Pi', 'So'];

    private static $calendar;

    public static function create()
    {
        return \Calendar::addEvents(self::getOccurrences())
            ->setOptions(self::getCalOptions())
            ->setCallbacks(self::getCalCallbacks());
    }

    public static function createwEvents(array $events)
    {
        return \Calendar::addEvents($events)
            ->setOptions(self::getCalOptions())
            ->setCallbacks(self::getCalCallbacks());
    }

    public static function createSmall()
    {
        $calendar = \Calendar::setOptions(self::getCalOptionsSmall());
        $calendar->setCallbacks(self::getCalCallbacksSmall($calendar));
        return $calendar;
    }

    private static function getCalOptions()
    {
        $openHrs[0] = \DB::table('opening_hrs')->find(1)->start_time;
        $openHrs[1] = \DB::table('opening_hrs')->find(3)->end_time;

        $calOptions = [
            'defaultView' => 'agendaWeek',
            'firstDay' => '1',
            'header' => [
                'left' => 'prev,next today',
                'center' => 'title',
                'right' => 'month,agendaWeek,agendaDay',
            ],
            'buttonText'=> [
                'today' => 'dnes',
                'month' => 'mesiac',
                'week' => 'týždeň',
                'day' => 'deň',
                'list' => 'zoznam'
            ],
            'height' => 'auto',
            'editable' => false,
            'monthNames' => self::$months,
            'dayNamesShort' => self::$days,
            'minTime' => $openHrs[0],
            'maxTime' => $openHrs[1],
            'timeFormat' => 'H:mm',
            'axisFormat' => 'H:mm',
            'eventLimit' =>false
        ];
        return $calOptions;
    }

    private static function getCalOptionsSmall()
    {
        $calOptions = [
            'firstDay' => '1',
            'header' => [
                'left' => 'prev,next,today',
                'center' => 'title',
                'right' => '',
            ],
            'buttonText'=> [
                'today' => 'dnes'
            ],
            'titleFormat' =>'MMMM YYYY',
            'weekMode' => 'variable',
            'height' => 'auto',
            'monthNames' => self::$months,
            'dayNamesShort' => self::$days,
            'timeFormat' => 'H:mm',
            'eventLimit' =>false,
        ];

        return $calOptions;
    }

    private static function getOccurrences()
    {
        $occurrences = [];
        $allOccrrs = \App\Occurrence::all();

        if($allOccrrs->count()) {

            $borderColor = [];

            for ($i = Place::all()->count()-1; $i >= 0 ; $i--)
            {
                //for every place compute one colour that will be used with occurrences held in the same place
                //this way we can say where is occurrence held just by its color
                $rgbArr = static::ColorHSLToRGB((($i * 120) % 360) / 360, 0.7, 0.5);
                $borderColor[$i] = 'rgb(' . intval($rgbArr['r']) . ',' . intval($rgbArr['g']) . ',' . intval($rgbArr['b']) . ')';
            }
//            dd($borderColor);

            foreach ($allOccrrs as $occurrence) {

//                dd($occurrence);

                $event = $occurrence->event()->first();

                if (\Auth::user() && \Auth::user()->isEmployee())
                {
                    $title = $event->title;
                    $url = url('/occurrences/' . $occurrence->id);
                    $isEditable = true;
                    if (\Auth::user()->isOrganiser($occurrence->id))
                    {
                        $color = $occurrence->confirmed ? "#71d662" : '#d66666';
                    }
                    else
                    {
                        $color = '#3b66aa';
                    }

                }
                else if (\Auth::user() && \Auth::user()->isAttendee($occurrence->id))
                {
                    $title = $event->title;
                    $url = url('/occurrences/' . $occurrence->id . '/attendee');
                    $color = $occurrence->confirmed ? "#71d662" : '#d66666';
                }
                else
                {
                    $title = empty($occurrence->place) ? 'Obsadene' : $event->title;
                    $url = '#';
                    $isEditable = false;
                    $color = '#3b66aa';

                }

                //if occurrence has no place set
                $borderColorId = empty($occurrence->place_id) ? 0 : $occurrence->place_id - 1;
                $occurrences[] = \Calendar::event(
                    $title,
                    false,
                    $occurrence->start_time,
                    $occurrence->end_time,
                    $occurrence->id,
                    // Add color and link on event
                    [
                        'color' => $color,
                        'url' => $url,
                        'borderColor' => $borderColor[$borderColorId],
                        'className' => 'moreBorder'
                    ]
                );
            }
        }
        return $occurrences;
    }

    private static function getCalCallbacks()
    {
        $calCallbacks = [

        ];
        return $calCallbacks;
    }


        private static function getCalCallbacksSmall($calendar)
    {

        $calCallbacks = [
            'dayClick' => 'function( date, jsEvent, view) 
            { 
                
               createEvent(date);
                
//                    $(this).css(\'background-color\', \'red\');
                
                console.log(\'Clicked on: \' + date.format());
            
//                console.log(\'Coordinates: \' + jsEvent.pageX + \',\' + jsEvent.pageY);
            
//                console.log(\'Current view: \' + view.name);
            
                
                
            }

'
        ];
        return $calCallbacks;
    }

//    private static function getCalCallbacksSmall()
//    {
//        $calCallbacks = [
//            'eventMouseover' => "function (event, jsEvent, view){
//                if(view.name === 'month'){
//                    console.log($(this).children('div').html());
//                }
//            }",

            // 'dayRender' => "function( date, cell ) {
            //     function getEvents(date){
            //         events.forEach(function(entry) {
            //             if (entry['start'] == date.format()){
            //                 alert(entry['title']);
            //             }
            //             else if (entry['start'] <= date.format() && entry['end'] >= date.format()){
            //                 alert(entry['title']);
            //             }
            //         });
            //     }

            //     // getEventSources();
            //     // console.log($(cell).get(0));
            //     // console.log($('.fc-time').get(0));
            // }",
//            'eventRender' => 'function( event, element, view ) {
//                $(element).find(".fc-time").hide();
//            }'
//
//        ];
//        return $calCallbacks;
//    }


    /**
     * Method converts color from HSL format to RGB format
     *
     * source: https://stackoverflow.com/a/20440417
     */
    public static function ColorHSLToRGB($h, $s, $l){

        $r = $l;
        $g = $l;
        $b = $l;
        $v = ($l <= 0.5) ? ($l * (1.0 + $s)) : ($l + $s - $l * $s);
        if ($v > 0){
            $m = null;
            $sv = null;
            $sextant = null;
            $fract = null;
            $vsf = null;
            $mid1 = null;
            $mid2 = null;

            $m = $l + $l - $v;
            $sv = ($v - $m ) / $v;
            $h *= 6.0;
            $sextant = floor($h);
            $fract = $h - $sextant;
            $vsf = $v * $sv * $fract;
            $mid1 = $m + $vsf;
            $mid2 = $v - $vsf;

            switch ($sextant)
            {
                case 0:
                    $r = $v;
                    $g = $mid1;
                    $b = $m;
                    break;
                case 1:
                    $r = $mid2;
                    $g = $v;
                    $b = $m;
                    break;
                case 2:
                    $r = $m;
                    $g = $v;
                    $b = $mid1;
                    break;
                case 3:
                    $r = $m;
                    $g = $mid2;
                    $b = $v;
                    break;
                case 4:
                    $r = $mid1;
                    $g = $m;
                    $b = $v;
                    break;
                case 5:
                    $r = $v;
                    $g = $m;
                    $b = $mid2;
                    break;
            }
        }
        return array('r' => $r * 255.0, 'g' => $g * 255.0, 'b' => $b * 255.0);
    }

}