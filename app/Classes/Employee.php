<?php
/**
 * Created by PhpStorm.
 * User: peter
 * Date: 5.3.2018
 * Time: 21:15
 */

namespace App\Classes;
use App\Classes\AvailableInterval as Interval;
use Carbon\Carbon;

class Employee
{//class not used in this version
    private $id;

    /**
     * This attr stores times when this user is available
     * to organise an event
     * @var
     */
    private $freeTimes;

    private $placeFreeTimes;

    /**
     * Employee constructor.
     * @param array $possibleTimes
     * @param $date
     */
    public function __construct(int $id, $availabilities, int $duration)
    {
//        $this->possibleTimes = [];
        $this->freeTimes = [];
        $this->id = $id;
        $this->placeFreeTimes = [];

        $this->findFreeHours($availabilities, $duration);
    }

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }

        return $this;
    }

    public function addFreeInterval(Interval $interval)
    {
        if (count($this->freeTimes) == 5){
            dump($interval);
        }
        $this->freeTimes[count($this->freeTimes)] = $interval;
    }

    public function addPlaceFreeInter(Interval $interval)
    {
        $this->placeFreeTimes[count($this->placeFreeTimes)] = $interval;
    }

}