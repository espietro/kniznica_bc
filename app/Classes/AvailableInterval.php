<?php
/**
 * Created by PhpStorm.
 * User: peter
 * Date: 5.3.2018
 * Time: 1:46
 */

namespace App\Classes;

use App\Availability;
use \Carbon\Carbon;
use Symfony\Component\Translation\Interval;

/**
 * Class AvailableInterval
 * @package App\Classes
 * Class works as temporary storage, it holds time of employee`s availability that corresponds to place availability for some event.
 */
class AvailableInterval
{
    private $startTime;
    private $endTime;
    private $placeId;
    private $userId;
    private $eventId;


    public function __construct(string $startTime, string $endTime, int $placeId, int $userId, int $eventId)
    {
        $this->startTime = $startTime;
        $this->endTime = $endTime;
        $this->placeId = $placeId;
        $this->userId = $userId;
        $this->eventId = $eventId;
    }

    public static function withCarbon(Carbon $startTime, Carbon $endTime, int $placeId, int $userId, int $eventId)
    {
        return new self($startTime->toDateTimeString(), $endTime->toDateTimeString(), $placeId, $userId, $eventId);
    }

    public static function withAvailab($availab, int $placeId, int $eventId)
    {
        return new self($availab->start_time, $availab->end_time, $placeId, $availab->user_id, $eventId);
    }

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }

        return $this;
    }

    /*
     * Function tests if specified duration can fit inside this availableInterval
     */
    public function canFit(int $duration)
    {
        $start = Carbon::parse($this->startTime);
        $end = Carbon::parse($this->endTime);
        return $start->lt($end) && $start->diffInMinutes($end) >= $duration;
    }
}