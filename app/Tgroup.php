<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tgroup extends Model
{
    protected $fillable = ['name'];

    
    /**
    *   TargetGroup je vo vztahu s Event, 1:N
    */
    public function events()
    {
        return $this->belongsToMany(Event::class);
    }


    
}
