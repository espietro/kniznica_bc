<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;
use App\Classes\AvailableInterval as Interval;
use App\Classes\Employee;

use App\Notifications\MyResetPassword;

class User extends Authenticatable
{
    use Notifiable;

    public static $NO_ROLE= 0;
    public static $USER = 2;
    public static $EMPLOYEE = 3;
    public static $ADMIN = 1;
    public static $CONTACT_PERSON = 4;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'phone', 'institution'];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];


    /**
    *   User je vo vztahu s Event, 1:N
    */
    public function events()
    {
        return $this->belongsToMany(Event::class);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function occurrences()
    {
        return $this->hasMany(Occurrence::class);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function organiseOccurrences()
    {
        return $this->hasMany(Occurrence::class, 'organiser_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }


    /**
     * @return bool true if this user is admin or employee
     */
    public function isEmployee()
    {
        foreach ($this->roles as $role) {
            if ($role->id == self::$ADMIN || $role->id == self::$EMPLOYEE) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return bool true if this user is admin
     */
    public function isAdmin()
    {
        foreach ($this->roles as $role) {
            if ($role->id == self::$ADMIN) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param int $eventId
     * @return bool true if this user is creator of specified event
     */
    public function isCreator(int $eventId)
    {
        $record = \DB::table('event_user')->where([
            ['user_id', $this->id],
            ['event_id', $eventId]
        ])->first();

        return isset($record->is_creator) ? $record->is_creator : false;
    }

    /**
     * @param int $occrrId
     * @return bool true if this user organise specified occurrence
     */
    public function isOrganiser(int $occrrId)
    {
        $record = $this->organiseOccurrences()->where('id', $occrrId)->get();

        return count($record) == 1;
    }


    /**
     * @param int $occrrId
     * @return bool true if this user attends specified occurrence
     */
    public function isAttendee(int $occrrId)
    {
        $record = $this->occurrences()->where('id', $occrrId)->get();

        return count($record) == 1;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function availabilities()
    {
        return $this->hasMany(Availability::class);
    }

    /**
     * Used to send customised (translated) reset password emails to users.
     * @param string $token
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MyResetPassword($token));
    }

    public function isContactPerson()
    {
        $contact = Role::find(User::$CONTACT_PERSON)->users()->first();
        return $contact != null ? (Role::find(User::$CONTACT_PERSON)->users()->first()->id == $this->id) : false;
    }

}
