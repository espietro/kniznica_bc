<?php

namespace App;
use App\Classes\Employee;
use App\Classes\MyMinHeap;
use Illuminate\Database\Eloquent\Model;
use App\Classes\AvailableInterval as Interval;
use Carbon\Carbon;

class Place extends Model
{
    protected $fillable = ['name'];

    /**
    *   Place je vo vztahu s Occurence, 1:N
    */
    public function occurrences()
    {
        return $this->hasMany(Occurrence::class);
    }

    public function events()
    {
        return $this->belongsToMany(Event::class);
    }

    public function occupancies()
    {
        return $this->hasMany(Occupancy::class);
    }

}
