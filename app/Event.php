<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

    //specified items are like whitelist for mass assignment
    protected $fillable = ['id','title', 'duration', 'subject'];

    /**
    *   Event je vo vztahu s Occurence, M:N
    */
    public function occurrences()
    {
    	// return $this->belongsToMany(Occurence::class);
        return $this->hasMany(Occurrence::class);
    }

    /**
    *   Event je vo vztahu s User, N:1
    */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
    *   Event je vo vztahu s Target Group, M:N
    */
    public function tgroups()
    {
        return $this->belongsToMany(Tgroup::class);
    }

    public function places()
    {
        return $this->belongsToMany(Place::class);
    }

    /**
     * Function is called like this: $event->creator
     */
    public function getCreatorAttribute()
    {
        return $this->users()->where('is_creator', true)->first();
    }
}
