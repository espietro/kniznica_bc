<?php

namespace App\Mail;

use App\Occurrence;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CancelOrder extends Mailable
{
    use Queueable, SerializesModels;

    protected $occurr;
//    protected $receiver;
    protected $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(/*User $receiver,*/ Occurrence $occurr, $message)
    {
        $this->occurr = $occurr;
//        $this->receiver = $receiver;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        self::subject('Zrušenie Rezervácie');
        return $this->view('emails.onCancelOrder')
            ->from(auth()->user())
            ->with([
                'occrr' => $this->occurr,
//                'user' => $this->receiver,
                'myMessage' => $this->message
            ]);
    }
}
