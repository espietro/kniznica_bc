<?php

namespace App\Mail;

use App\Occurrence;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Order extends Mailable
{
    use Queueable, SerializesModels;

    protected $sender;
    protected $recipient;
    protected $occrr;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $sender, User $recipient, Occurrence $occrr)
    {
        $this->sender = $sender;
        $this->recipient = $recipient;
        $this->occrr = $occrr;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        self::subject('Rezervácia');
        if ($this->sender->isEmployee())
        {
            return $this->view('emails.onOrder')
                ->from($this->sender)
                ->with([
                    'sender' => $this->sender,
                    'recipient' => $this->recipient,
                    'occrr' => $this->occrr
                ]);
        }
        return $this->view('emails.onOrderEmp')
            ->from($this->sender)
            ->with([
                'sender' => $this->sender,
                'recipient' => $this->recipient,
                'occrr' => $this->occrr
            ]);


    }
}
