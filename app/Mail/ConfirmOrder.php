<?php

namespace App\Mail;

use App\Occurrence;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConfirmOrder extends Mailable
{
    use Queueable, SerializesModels;

    protected $occurr;
    protected $organiser;
    protected $message;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $organiser, Occurrence $occurr, $message)
    {
        $this->occurr = $occurr;
        $this->organiser = $organiser;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        self::subject('Potvrdenie Rezervácie');
        return $this->view('emails.onConfirmOrder')
            ->from($this->organiser)
            ->with([
                'occrr' => $this->occurr,
                'myMessage' => $this->message
            ]);
    }
}
