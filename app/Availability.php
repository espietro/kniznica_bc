<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Availability extends Model
{
    public $timestamps = false;

    protected $fillable = ['user_id', 'start_time', 'end_time'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function activities()
    {
        return $this->hasMany(Activity::class);
    }
}
