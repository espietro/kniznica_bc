<?php

namespace App\Providers;
/*vv*/
use Illuminate\Support\Facades\Schema;
/*^^*/
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        /*vv*/
        Schema::defaultStringLength(191);
        /*^^*/
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
