<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    public function availability()
    {
        return $this->belongsTo(Availability::class);
    }
}
