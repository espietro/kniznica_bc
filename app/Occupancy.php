<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Occupancy extends Model
{
    protected $fillable = ['start_time','end_time','place_id'];
    
    public function place()
    {
        return $this->belongsTo(Place::class);
    }
}
