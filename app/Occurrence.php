<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Occurrence extends Model 
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['event_id',
        'organiser_id',
        'place_id',
        'user_id',
        'start_time',
        'end_time',
        'confirmed'
    ];

    /**
    *   Occurrence je vo vztahu s Place, N:1
    */
    public function place()
    {
        return $this->belongsTo(Place::class);
    }

    /**
    *   Occurrence je vo vztahu s User, N:1
    */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function organiser()
    {
        return $this->belongsTo(User::class, 'organiser_id');
    }

    /**
    *   Occurrence je vo vztahu s Event, M:N
    */
    public function event()
    {
        return $this->belongsTo(Event::class);        
    }

}
