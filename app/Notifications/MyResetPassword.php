<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MyResetPassword extends \Illuminate\Auth\Notifications\ResetPassword//extends Notification
{

//    use Queueable;
//
//    /**
//     * Create a new notification instance.
//     *
//     * @return void
//     */
//    public function __construct()
//    {
//        //
//    }
//
//    /**
//     * Get the notification's delivery channels.
//     *
//     * @param  mixed  $notifiable
//     * @return array
//     */
//    public function via($notifiable)
//    {
//        return ['mail'];
//    }
//
    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Obnova hesla')
            ->from('noreply@krajskakniznicazilina.sk')
            ->greeting('Dobrý deň,')
            ->line('posielame Vám tento email, pretože sme obdržali Vašu žiadosť o obnovu hesla.')
            ->action('Obnoviť Heslo', url(config('app.url').route('password.reset', $this->token, false)))
            ->line('Ak ste o zmenu hesla nežiadali, nie je potrebné vykonať žiadne dalšie akcie a tento email považujte za bezpredmetný.');
//            ->salutation('S pozdravom </br>' . config('app.name'))
    }
//
//    /**
//     * Get the array representation of the notification.
//     *
//     * @param  mixed  $notifiable
//     * @return array
//     */
//    public function toArray($notifiable)
//    {
//        return [
//            //
//        ];
//    }
}
