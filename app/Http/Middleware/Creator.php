<?php

namespace App\Http\Middleware;

use Closure;

class Creator
{
    /**
     * Handle an incoming request. Only allow if user has created given event.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $event = $request->route('event');

        if (isset($event->id)){
            $event = $event->id;
        }
        if (\Auth::check()
            && \Auth::user()->isEmployee()
            && \Auth::user()->isCreator($event)
            || \Auth::user()->isAdmin())
        {
            return $next($request);
        }
        return redirect()->intended('/home');
    }
}
