<?php

namespace App\Http\Middleware;

use Closure;

class Employee
{
    /**
     * Handle an incoming request. Only allow if user is employee.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$guards)
    {
        if (\Auth::check() && \Auth::user()->isEmployee())
        {
            return $next($request);
        }
        return redirect()->intended('/home');
    }
}
