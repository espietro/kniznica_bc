<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;    //->kvoli Auth::user()
use App\Event;
use App\Occurrence;
use App\Tgroup;
use App\Place;
use Calendar;

class EventsController extends Controller
{
    private static $months = ['Január', 'Február', 'Marec', 'Apríl', 'Máj', 'Jún', 'Júl', 'August', 'September', 'Október', 'November', 'December'];
    private static $days = ['Ne', 'Po', 'Ut', 'St', 'Št', 'Pi', 'So'];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $cur_page = "events";
        $events = Event::all();

        //this is going to look for the view in resources/views directory
        return view('events/index', compact(["events", "cur_page"/*, "calendar"*/]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tgroups = \DB::table('tgroups')->get();
        $places = \DB::table('places')->where('id','>',0)->get();
        $employees = \DB::table('users')->join('role_user', 'users.id', '=', 'role_user.user_id')->where('role_id', 1)->orWhere('role_id',3)->get();
        return view ('events/create', compact(['tgroups', 'places', 'employees']));
    }

    /**
     * Store a newly created event in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(),[
            'title' => 'required'
        ]);
        $event = Event::create([
            'title' => request('title'),
            'duration' => request('duration'),
            'subject' => request('subject')
        ]);
        if (request('visible'))
        {
            $event->visible = false;
        }
        $event->tgroups()->attach(request('tgroup'));
        $event->places()->attach(request('place'));
        $event->users()->attach(request('employee'));
        $event->users()->attach(auth()->id(), ['is_creator' => true]);
          
        if($event->save())
        {
            session()->flash('message', 'Podujatie bolo pridané.');
        }
        else
        {
            session()->flash('warning', 'Podujatie sa nepodarilo vytvoriť.');
        }
        return redirect('/events');
    }

    /**
     * Display the specified event.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::findOrFail($id);
        return view('events/detail', compact(["event", "cur_page"]));
    }

    /**
     * Show the form for editing the specified event.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::findOrFail($id);
        $tgroups = Tgroup::all();
        $places = Place::all();
        $employees = \DB::table('users')->join('role_user', 'users.id', '=', 'role_user.user_id')->where('role_id', 1)->orWhere('role_id',3)->get();
        return view('events/edit', compact(['event', 'tgroups', 'places','employees']));
    }

    /**
     * Update the specified event in database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $event = Event::findOrFail($id);
        $creator_id = $event->creator->id;
        $this->validate(request(),[
            'title' => 'required'
        ]);

        $event->update([
            'title' => request('title'),
            'user_id' => auth()->id(),
            'subject' => request('subject')
        ]);

        if (\request('visible'))
        {
            $event->visible = false;
        }
        else
        {
            $event->visible = true;
        }

        $event->tgroups()->sync(request('tgroup'));
        $event->places()->sync(request('place'));
        $event->users()->sync(request('employee'));
        $event->users()->attach($creator_id);
        $event->users()->updateExistingPivot($creator_id, ["is_creator"=>true]);
        if($event->save())
        {
            session()->flash('message', 'Podujatie bolo úspešne upravené.');
        }
        else
        {
            session()->flash('warning', 'Podujatie sa nepodarilo upraviť.');
        }
        return redirect('/events');
    }

    /**
     * Remove the specified event from database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        $items = $event->occurrences;
        if (count($items))
        {
            session()->flash('warning', 'Podujatie nebolo zmazané, pretože sa vyskytuje v niektorých udalostiach.');
            return redirect('/events');
        }
        else
        {
            if($event->delete())
            {
                session()->flash('message', 'Podujatie bolo úspešne zmazané.');
            }
            else
            {
                session()->flash('warning', 'Podujatie sa nepodarilo zmazať.');
            }
            return redirect('/events');
        }
    }


    /**
     * Show form with my events to change them its creator to someone else.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function creator()
    {
        $action = '/events/creator';
        $title = 'Zmeniť autora vybratým podujatiam';
        $users = User::join('role_user', 'user_id', 'id')->whereIn('role_id',[1,3])->get();
        $events = \auth()->user()->events()->where('is_creator',true)->get();

        return view('/events/creator', compact(['action', 'title', 'users', 'events']));
    }


    /**
     * Change creator for specified events.
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateCreator()
    {
        $eventIds=\request('events');
        $events = Event::find($eventIds);
        $oldCreator = \auth()->user();
        $newCreator = User::find(\request('newCreator'));

        $oldCreator->events()->detach($eventIds);

        $newCreator->events()->syncWithoutDetaching(array_combine($eventIds, array_fill(0,count($eventIds), ['is_creator' => true])));

        return redirect('/events/creator');

    }

}
