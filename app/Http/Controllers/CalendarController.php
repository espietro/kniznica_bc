<?php

namespace App\Http\Controllers;

use App\Classes\Calendar;
use Illuminate\Http\Request;
use App\Occurrence;

class CalendarController extends Controller
{



    public function index()
    {
        $cur_page = "calendar";
        $calendar = Calendar::create();
        $cal_id = $calendar->getId();
                        
        return view('calendar/fullcalendar', compact(['calendar', 'cur_page', 'cal_id']));
    }






}
