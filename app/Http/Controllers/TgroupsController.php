<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tgroup;

class TgroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Tgroup::all();
        $title = 'Zmazať cieľovú skupinu';
        $action = url('/tgroups/destroy');
        $label = 'tgroups';
        $question = 'Ktorú cieľovú skupinu?';
        return view('/showBeforeDelete', compact(['items','title', 'action', 'label', 'question']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tgroups = Tgroup::all();
        return view('tgroups/create', compact(['tgroups']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->validate(request(),[
            'tgroup' => 'required'
        ]);


        $tgroup = Tgroup::create([
            'name' => request('tgroup')
        ]);

        if ($tgroup->save())
        {
            session()->flash('message', 'Cieľová skupina bola vytvorená.');
        }
        else
        {
            session()->flash('warning', 'Cieľovú skupinu sa nepodarilo vytvoriť.');
        }


        return redirect('/events');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $tgroup = Tgroup::find(\request('tgroups'));
        if($tgroup->delete())
        {
            session()->flash('message', 'Cieľová skupina bola úspešne zmazaná.');
        }
        else
        {
            session()->flash('warning', 'Cieľovú skupinu sa nepodarilo zmazať.');
        }
        return redirect('/events');
    }
}
