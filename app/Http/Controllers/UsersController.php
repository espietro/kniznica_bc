<?php

namespace App\Http\Controllers;

use App\Availability;
use App\Classes\Calendar;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = \Auth::user();
        $availabs = $user->availabilities;

        $occurrs = $user->organiseOccurrences;

        $items = [];
        if($availabs->count()) {
            foreach ($availabs as $availab) {

                $url = url('/occurrences/'. $availab->id  .'/createBlock');
                $items[] = \Calendar::event(
                    $user->name,
                    false,
                    $availab->start_time,
                    $availab->end_time,
                    $availab->id,
                    // Add color and link on event
                    [
                        'color' => '#7a8089',
                        'url' => $url
                    ]
                );
            }
            if ($occurrs->count())
            {
                foreach ($occurrs as $occurr)
                {
                    $url = url('/occurrences/' . $occurr->id);
                    $color = $occurr->confirmed ? "#71d662" : '#d66666';

                    $items[] = \Calendar::event(
                        $occurr->event->title,
                        false,
                        $occurr->start_time,
                        $occurr->end_time,
                        null,
                        // Add color and link on event
                        [
                            'color' => $color,
                            'url' => $url
                        ]
                    );
                }
            }
        }


        $calendar = Calendar::createwEvents($items);
        $options = $calendar->getOptions();
        $options['editable'] = false;

        $calendar->setOptions($options);


        return view('/availability/calendar', compact(['calendar', 'user']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Availability $availab)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $roles = Role::where('id', '<>', User::$CONTACT_PERSON)->get();
        $users = User::all();
        $contact = Role::find(User::$CONTACT_PERSON)->users()->first();
        $contactId = $contact != null ? Role::find(User::$CONTACT_PERSON)->users()->first()->id : 0;
        return view('/users/edit', compact(['roles','users', 'contactId']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        $users = User::find(\request('users'));
        $role = \request('roles');

        if(count($users) < 1)
        {
            session()->flash('warning', 'Nevybrali ste žiaden účet.');
            return redirect('/users/edit');
        }

        // if "odobrat prava" option was selected
        if ($role == User::$NO_ROLE)
        {
            foreach ($users as $user)
            {
                //there has to be at least one last administrator
                if ($user->isAdmin() && \DB::table('role_user')->where('role_id', User::$ADMIN)->count() == 1)
                {
                    session()->flash('warning', 'Odoberáte právo ' . Role::find(User::$ADMIN)->name . ' používateľovi ' . $user->name . ', v systéme musí zostať aspoň jeden administrátorský účet. Rola nebola odobraná.');
                    continue;
                }

                if ($user->isContactPerson())
                {
                    session()->flash('warning', 'Odoberáte právo ' . Role::find(User::$CONTACT_PERSON)->name . ' používateľovi ' . $user->name . ', v systéme musí zostať presne jeden kontaktný účet. Rola nebola odobraná.');
                    continue;
                }

                $user->roles()->sync(User::$USER);

            }
            session()->flash('message', 'Práva boli úspešne odobrané.');
        }
        else
        {

            foreach ($users as $user)
            {
                //there has to be at least one last administrator
                if ($user->isAdmin() && \DB::table('role_user')->where('role_id', User::$ADMIN)->count() == 1 && ($role != User::$ADMIN))
                {
                    session()->flash('warning', 'Odoberáte právo ' . Role::find(User::$ADMIN)->name . ' používateľovi ' . $user->name . ', v systéme musí zostať aspoň jeden administrátorský účet.');
                }
                else if ($user->isContactPerson() && $role == User::$USER)
                {
                    session()->flash('warning', 'Odoberáte právo ' . Role::find(User::$CONTACT_PERSON)->name . ' používateľovi ' . $user->name . ', v systéme musí zostať presne jeden kontaktný účet.');
                }
                else
                {
                    if ($user->isContactPerson())
                    {
                        $user->roles()->sync($role);
                        $user->roles()->attach(User::$CONTACT_PERSON);
                    }
                    else
                    {
                        $user->roles()->sync($role);
                    }
                }
            }
            session()->flash('message', 'Práva boli úspešne zmenené.');
        }
        return redirect('/users/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if ($user->id == auth()->user()->id)
        {
            session()->flash('warning', 'Nemôžete odstrániť vlastný účet.');
        }
        else
        {
            //there has to be at least one last administrator
            if ($user->isAdmin() && \DB::table('role_user')->where('role_id', 1)->count() == 1)
            {
                session()->flash('warning', 'Nemôžete odstrániť posledný administrátorský účet.');
            }
            else if ($user->isContactPerson())
            {
                session()->flash('warning', 'Nemôžete odstrániť kontaktný účet, najskôr vyberte iného zamestnanca ako kontaktnú osobu.');
            }
            else
            {
                try
                {
                    $user->delete();
                    session()->flash('message', 'Užívateľ bol úspešne odstránený.');
                }
                catch (\Exception $e)
                {
                    session()->flash('warning', 'Účet sa nepodarilo odstrániť');
                }
            }
        }


        return redirect('/users/edit');
    }


    public function setContactPerson(User $user)
    {
        if ($user->isEmployee())
        {
            $oldContact = Role::find(User::$CONTACT_PERSON)->users()->first();
            if ($oldContact != null)
            {
                $oldContact->roles()->detach(User::$CONTACT_PERSON);
            }
            $user->roles()->attach(User::$CONTACT_PERSON);
        }
        else
        {
            session()->flash('warning', 'Kontaktná osoba musí byť zamestnanec.');
        }
        return redirect('/users/edit');
    }
}
