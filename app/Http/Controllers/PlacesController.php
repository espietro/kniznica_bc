<?php

namespace App\Http\Controllers;

use App\Occurrence;
use App\Place;
use Illuminate\Http\Request;

class PlacesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Place::all()->where('id', '>', 0);
        $title = 'Zmazať miestnosť';
        $action = url('/places/destroy');
        $label = 'places';
        $question = 'Ktorú miestnosť?';
        $info = 'Táto akcia nenávratne zmaže aj všetky udalosti ktoré sú naplánované v tejto miestnosti.';
        return view('/showBeforeDelete', compact(['items','title', 'action', 'label', 'question', 'info']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('places/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(),[
            'place' => 'required'
            // 'employee' => 'required'
        ]);

        $place = Place::create([
            'name' => request('place')
        ]);
        if ($place->save())
        {
            session()->flash('message', 'Miestnosť bola úspešne vytvorená.');
        }
        else
        {
            session()->flash('warning', 'Miestnosť sa nepodarilo vytvoriť.');
        }

        return redirect('/events');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $place = Place::find(\request('places'));

        if ($place->occurrences->count()){
            foreach ($place->occurrences as $occurrence)
            {
                $occurrence->delete();
            }
        }

        try
        {
            $place->delete();
            session()->flash('message', 'Miestnosť bola úspešne zmazaná.');
        }
        catch (\Exception $e)
        {
            session()->flash('warning', 'Miestnosť sa nepodarilo zmazať.');
        }

        return redirect('/events');
    }
}
