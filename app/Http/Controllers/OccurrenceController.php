<?php

namespace App\Http\Controllers;

use App\Availability;
use App\Classes\MyMinHeap;
use App\Mail\CancelOrder;
use App\Mail\ConfirmOrder;
use App\Mail\Order;
use App\Occurrence;
use App\Tgroup;
use App\User;
use Illuminate\Http\Request;
use App\Event;
use App\Occupancy;
use App\Place;
use Carbon\Carbon;
use App\Classes\AvailableInterval as Interval;
use App\Classes\Employee;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use function PHPSTORM_META\type;


class OccurrenceController extends Controller
{
    //class will be split in next version, new controller for block occurrence is needed

    /**
     * Display form to choose event.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::all();
        return view('occurrences/selectEvent', compact('events'));
    }

    /**
     * Display form to choose target groups
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function indexNoEvent()
    {
        $tgroups = Tgroup::all();
        return view('occurrences/selectNoEvent', compact('tgroups'));
    }

    /**
     * Show the form for creating a new occurrence.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $event = \App\Event::find(request('eventId'));
        $interval = new Interval(\request('startTime'), \request('endTime'), request('placeId'), request('userId'), \request('eventId'));
        return view('occurrences/create', compact(['interval', 'event']));
    }

    /**
     * Show the form for creating new block occurrence.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createBlock()
    {
        $user = \Auth::user();
        $title = 'Vytvoriť Blok';
        $name = 'Názov';
        $subject = 'Popis';
        $action = '/occurrences/createBlock';
        $availab = Availability::findOrFail(\request('availab'));
        $startTime = Carbon::parse($availab->start_time);
        $endTime = Carbon::parse($availab->end_time);
        return view('/availability/create', compact(['user', 'title', 'action', 'startTime', 'endTime', 'name', 'subject']));
    }

    /**
     * Store a newly created occurrence in database.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = \App\Event::findOrFail(request('eventId'));

        $date = Carbon::parse(\request('date'));

        $startTime = $date->setTimeFromTimeString(\request('startTime'));
        $endTime = $startTime->copy()->addMinutes($event->duration);

        $organiser = \App\User::findOrFail(\request('userId'));

        $placeId = \request('placeId');

        $user = Auth::user();

        //check if it is possible to handle new occurrence at specified place
        if ($this->checkAvailability($organiser, $startTime, $endTime, $event, $placeId))
        {
            $occrr = Occurrence::create([
                'event_id' => $event->id,
                'organiser_id' => $organiser->id,
                'place_id' => $placeId,
                'user_id' => $user->id,
                'start_time' => $startTime->toDateTimeString(),
                'end_time' => $endTime->toDateTimeString(),
                'confirmed' => false
            ]);
            if($occrr->save())
            {
                try
                {
                    Mail::to($user)->send(new Order($organiser, $user, $occrr));
                    Mail::to($organiser)->send(new Order($user, $organiser, $occrr));
                }
                catch (\Exception $e)
                {
                    session()->flash('warning', 'Ospravedlňujeme sa, email sa nepodarilo odoslať, na oprave tejto chyby pracujeme. Vďaka za pochopenie.');
                }
                session()->flash('message', 'Podujatie bolo rezervované. Bol vám odoslaný mail s detailami rezervácie. V najbližšej dobe Vás bude kontaktovať pracovník s potvrdením Vašej rezervácie.');
            }
            else
            {
                session()->flash('warning', 'Podujatie sa nepodarilo zarezervovať.');
            }

        }

        return redirect('/calendar');

    }

    /**
     * Store a newly created block occurrence in database.
     *
     * @param int|null $userId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeBlock(int $userId = null)
    {
        $userId = isset($userId) ?: Auth::user()->id;
        $user = User::findOrFail(\request('userId'));
        $day = Carbon::parse(\request('date'));
        $name = \request('name');
        $subject = \request('subject');
        $startTime = $day->copy()->setTimeFromTimeString(\request('startTime'));
        $endTime = $day->copy()->setTimeFromTimeString(\request('endTime'));

        if ($startTime->gte($endTime))
        {
            $endTime = $startTime->copy()->addMinute();
        }

        $event = Event::create([
            'title' => $name,
            'subject' => $subject,
        ]);

        $event->visible = false;
        $event->save();

        $event->users()->attach($userId, ['is_creator' => true]);


        $occrr = Occurrence::create([
            'event_id' => $event->id,
            'organiser_id' => $userId,
            'start_time' => $startTime->toDateTimeString(),
            'end_time' => $endTime->toDateTimeString(),
            'confirmed' => true
        ]);

        if ($occrr->save())
        {
            session()->flash('message', 'Blok bol úspešne vytvorený.');
        }
        else
        {
            session()->flash('warning', 'Blok sa nepodarilo vytvoriť.');
        }
        return redirect('/calendar');
    }

    /**
     * Display the specified occurrence details.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Occurrence $occrr)
    {
        return view('/occurrences/detail', compact('occrr'));
    }

    /**
     * Display specified occurrence details to attendee.
     *
     * @param Occurrence $occrr
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showAttendee(Occurrence $occrr)
    {
        return view('/occurrences/detail', compact('occrr'));
    }

    /**
     * Show form to edit mail for attendee to confirm occurrence.
     *
     * @param Occurrence $occrr
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirm(Occurrence $occrr)
    {
        $title = 'Potvrdiť udalosť';
        $action = '/occurrences/' . $occrr->id . '/confirm';
        $myMessage = 'Vaša rezervácia, na podujatie, ' . $occrr->event->title . ', bola potvrdená.
    Tešíme sa na Vás.';
        return view('emails/message', compact(['occrr', 'title', 'action', 'myMessage']));
    }

    /**
     * Send submited mail to attendee.
     *
     * @param Occurrence $occrr
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function confirmStore(Occurrence $occrr)
    {
        $message = \request('message');
        $occrr->confirmed = true;
        if ($occrr->save())
        {
            try {
                Mail::to($occrr->user)->send(new ConfirmOrder($occrr->organiser, $occrr, $message));
            }
            catch (\Exception $e)
            {
                session()->flash('warning', 'Ospravedlňujeme sa, email sa nepodarilo odoslať, na oprave tejto chyby pracujeme. Vďaka za pochopenie.');
            }
        }
        return redirect('/users/availabs');
    }

    /**
     * Delete block occurrence or show form to edit mail to be sent to attendee that occurrence is going to be canceled.
     *
     * @param Occurrence $occrr
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \Exception
     */
    public function destroyMessage(Occurrence $occrr)
    {

        if (empty($occrr->place_id) && empty($occrr->user))
        {
//            this occurrence was a BLOCK so we can also delete its event
            $occrr->event()->delete();
            if ($occrr->delete()) {
                session()->flash('message', 'Blok bol úspešne zrušený.');
            }
            else
            {
                session()->flash('warning', 'Blok sa nepodarilo zrušiť.');
            }
            return redirect('/calendar');
        }
        else {

            $title = 'Zrusiť udalosť';
            $action = '/occurrences/' . $occrr->id . '/cancel';
            $myMessage = null;
            if (\auth()->user()->isEmployee()) {
                $myMessage = 'Je nám to ľúto ale Vašu rezerváciu na podujatie, ' . $occrr->event->title . ', sme nemohli potvrdiť pretože: ... ';
            } else {
                $myMessage = 'Ruším rezerváciu podujatia. Sem môžete napísať dôvod, prečo ... ';
            }
            return view('emails/message', compact(['occrr', 'title', 'action', 'myMessage']));
        }
    }

    /**
     * Remove the specified occurrence from database.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (\Auth::user()->isOrganiser($id) || Auth::user()->isAttendee($id))
        {
            $occrr = Occurrence::findOrFail($id);

            if ($occrr->delete()) {
                try {
                    Mail::to(\auth()->user()->isEmployee() ? $occrr->user : $occrr->organiser)
                        ->send(new CancelOrder($occrr, \request('message')));
                } catch (\Exception $e) {
                    session()->flash('warning', 'Ospravedlňujeme sa, email sa nepodarilo odoslať, na oprave tejto chyby pracujeme. Vďaka za pochopenie.');
                }

                //there is also record in occupancies table that is deleted by db trigger
                session()->flash('message', 'Udalosť bola úspešne zrušená.');
            }
            else
            {
                session()->flash('warning', 'Udalosť sa nepodarilo zrušiť.');
            }
            return redirect('/calendar');
        }
        else
        {
            session()->flash('warning', 'Túto udalosť nemáte právo zrušiť, pretože nie ste jej organizátor ani účastník.');
            return redirect('/calendar');
        }
    }

    /**
     * Calculate intervals that user can reserve from given target groups, with no specified event.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reservationNoEvent(Request $request)
    {
        $tgroups = \request('tgroups');
        $day = carbon::parse(\request('startDay'));

        $tgroups = Tgroup::find($tgroups);

        $events = \App\Event::where('events.visible', true)->join('event_tgroup', 'events.id', '=', 'event_tgroup.event_id')->whereIn('tgroup_id', $tgroups)->get();

        $availabs = [];
        foreach ($events as $event)
        {
            //get employees` availabilities
            $availab = $this->getEmpAvailIntrvlsAsArray($day, $day, $event);
            if (count($availab))
            {
                $availabs = array_merge($availabs , $availab);
            }
        }

        $intrvls = new myMinHeap(true);

        foreach ($events as $event)
        {
            //check employees` availabilities with places` occupations
            $intrvls = $this->checkPlaceAvailsWithEmpAvails($day, $day, $event, $availabs, $intrvls);
        }

        // get rid of duplicates that user is not interested in seeing
        $intrvls = $this->eliminateEqualIntervals($intrvls);

        return view('occurrences/reservationNoEvent', compact(['intrvls', 'tgroups']));
    }

    /**
     * Calculate intervals that user can reserve, with specified event.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reservation(Request $request)
    {
        //start and end time where to look for instances
        $start = !empty(request('startDay')) ? Carbon::parse(request('startDay')) : Carbon::tomorrow();
        $end = !empty('endDay') ? Carbon::parse(request('endDay')) : $start->copy()->addDays(7);

        if (!$start->lte($end))
        {
            $end = $start->copy()->addDays(7);
        }

        // get id of the requested event
        $eventId = request('event');
        // find event that is going to be used for this occurrence in the database
        $event = Event::findOrFail($eventId);

        // get employees` availabilities
        $availabs = $this->getEmpAvailIntrvlsAsArray($start, $end, $event);
        // check availabilities with places` occupations
        $intrvls = $this->checkPlaceAvailsWithEmpAvails($start, $end, $event, $availabs);
        // get rid of duplicates that user is not interested in seeing
        $intrvls = $this->eliminateEqualIntervals($intrvls);


        return view('occurrences/reservation', compact('intrvls', 'eventId'));
    }

    /**
     * this method removes those intervals that has same start/end times but different places or organisers
     * thanks to this, if there were more equal time intervals before ,there will only be one interval for given time in the output
     */
    private function eliminateEqualIntervals(MyMinHeap $intrvls)
    {
        $array = null;
        foreach (clone $intrvls as $intrvl)
        {
            $array[] = $intrvl;
        }
        for ($i = 0; $i < count($array); $i++)
        {
            $one = Carbon::parse($array[$i]->startTime);
            for ($j = $i+1; $j < count($array); $j++)
            {
                $other = Carbon::parse($array[$j]->startTime);
                //we only compare those intervals that are in the same day
                if(!$one->isSameDay($other))
                {
                    //if one interval that we compare against others is less than others, we can immediately skip to next one, cause others are sorted by startTime
                    if ($other->gt($one))
                    {
                        continue 2;
                    }
                    continue;
                }

                if ($array[$i]->startTime === $array[$j]->startTime &&
                    $array[$i]->endTime === $array[$j]->endTime &&
                    $array[$i]->eventId === $array[$j]->eventId)
                {
                    //intervals has the same times and organisers
                    if ($array[$i]->userId === $array[$j]->userId)
                    {
                        unset($array[$j--]);
                        $array = array_values($array);
                    }
                    //intervals has the same times but different organisers
                    else
                    {   //we check if there is an organiser, and let customer choose him first time
                        if (User::find($array[$j]->userId)->isCreator($array[$j]->eventId))
                        {
                            $array[$i] = clone $array[$j];
                        }
                        unset($array[$j--]);
                        $array = array_values($array);
                    }
                }
            }
        }
        return $array;
    }

    /**
     * @param Carbon $start
     * @param Carbon $end
     * @param Event $event
     * @return array
     */
    private function getEmpAvailIntrvlsAsArray(Carbon $start, Carbon $end, Event $event)
    {
        $availabs = [];

        $curDay = $start->copy();

        while ($curDay->lte($end))
        {
            $availabs = $this->getEmployeesAvails($curDay, $event, $availabs);
            $curDay->addDay();
        }

        return $availabs;

    }

    /**
     * @param Carbon $start
     * @param Carbon $end
     * @param Event $event
     * @param array $availabs
     * @return MyMinHeap
     */
    private function checkPlaceAvailsWithEmpAvails(Carbon $start, Carbon $end, Event $event, array $availabs, MyMinHeap $intrvls = null)
    {
        $places = $event->places()->get();

        if ($start->eq($end))
        {
            $workDay = \DB::table('opening_hrs')->find($start->dayOfWeek);
            $start = $start->copy()->setTimeFromTimeString($workDay->start_time);
            $end = $start->copy()->setTimeFromTimeString($workDay->end_time);
        }

        if (!isset($intrvls))
        {
            $intrvls = new MyMinHeap();
        }

        $originalNumber = count($availabs);

        foreach ($places as $place)
        {
            $placeOccups = \DB::table('occupancies')
                ->where([
                    ['place_id', $place->id],
                    ['end_time', '>=', $start],
                    ['end_time', '<=', $end]])
                ->orderBy('start_time', 'asc')
                ->get();

            $availabs = array_slice($availabs, 0, $originalNumber);

            if (count($placeOccups))
            {

                for ($i = 0; $i < count($availabs); $i++)
                {
                    $avlStart = Carbon::parse($availabs[$i]->start_time);
                    $avlEnd = Carbon::parse($availabs[$i]->end_time);

                    foreach ($placeOccups as $plcOccup)
                    {

                        $ocpStart = Carbon::parse($plcOccup->start_time);
                        $ocpEnd = Carbon::parse($plcOccup->end_time);

                        //if occurrence and employee's availability has the same date we check them
                        if ($ocpStart->isSameDay($avlStart))
                        {

                            switch (true)
                            {
                                // [   ---]----  => [availability] -occupancy-
                                case ($ocpStart->between($avlStart, $avlEnd) && $ocpEnd->gt($avlEnd)):

                                    $avlEnd = $ocpStart->copy();
                                    break;

                                // ---[----   ]  => [availability] -occupancy-
                                case ($ocpEnd->between($avlStart, $avlEnd) && $ocpStart->lt($avlStart)):

                                    $avlStart = $ocpEnd->copy();
                                    break;

                                // [  ----   ]  => [availability] -occupancy-
                                case ($ocpStart->between($avlStart, $avlEnd) && $ocpEnd->between($avlStart, $avlEnd)):

                                    $avlEnd = $ocpStart->copy();
                                    $second = clone $availabs[$i];
                                    $second->start_time = $ocpEnd->toDateTimeString();
                                    array_push($availabs, $second);
                                    break;

                                // --[----]---  => [availability] -occupancy-
                                case ($avlStart->between($ocpStart, $ocpEnd) && $avlEnd->between($ocpStart, $ocpEnd)):
                                    continue 3; //jump to next availability

                                // [    ]  --- | ---  [   ] => [availability] -occupancy-
                                default:

                            }
                        }
                    }

                    $new = Interval::withCarbon($avlStart, $avlEnd, $plcOccup->place_id, $availabs[$i]->user_id, $event->id);

                    if ($new->canFit($event->duration))
                    {
                        $intrvls->insert($new);
                    }
                }
            }
            else
            {
                foreach ($availabs as $availab)
                {
                    if (count($event->users()->where('user_id', $availab->user_id)->get()))
                    {
                        $new = Interval::withAvailab($availab, $place->id, $event->id);
                        if ($new->canFit($event->duration))
                        {
                            $intrvls->insert($new);
                        }
                    }

                }
            }
        }

        return $intrvls;
    }

    /**
     * @param Carbon $day The day in which we want to find available free time of every employee.
     * @param Event $event The event that we want to find available employees for.
     * @param array $availabs Array of already found employees, where we should add those we find in given day.
     * @return array Array of found available employees for given day, with already existing employees as it was passed as parameter.
     */
    private function getEmployeesAvails(Carbon $day, Event $event, array $availabs)
    {

        //times of work for given day
        $workDay = \DB::table('opening_hrs')->find($day->dayOfWeek);

        //start and end time of work for given day
        $startDayTime = $day->copy()->setTimeFromTimeString($workDay->start_time);
        $endDayTime = $day->copy()->setTimeFromTimeString($workDay->end_time);

        // fetch those availabilities of employees for given event that fit inside given working times
        $empsAvails = \DB::table('availabilities')
            ->whereIn(
                'user_id', $event->users()->pluck('id'))
            ->where([
                ['end_time', '>=', $startDayTime],
                ['end_time', '<=', $endDayTime]])
            ->orderBy('start_time', 'asc')
            ->get();
//        dd($startDayTime, $endDayTime, $empsAvails, $event->users()->pluck('id'));

        foreach ($empsAvails as $empAvail)
        {
            //for every availability fetch occurrences that are inside this availability
            //occurrences are intervals of time during employee's availability i.e. when he is at work and organises some event
            $empOccurrs = \DB::table('occurrences')
                ->where([
                    ['organiser_id', $empAvail->user_id],
                    ['end_time', '>=', $startDayTime],
                    ['end_time', '<=', $endDayTime]])
                ->orderBy('start_time', 'asc')
                ->get();


            if (count($empOccurrs))
            {   //if there are occurrences, we have to split this availability and add only free time, i.e. when occurrence is not colliding with availability
                $availabs = $this->getEmployeeAvails($empOccurrs, $empAvail, $availabs, $event->duration);
            } else
            {   //if there are no occurrences we add whole employee's availability and continue
                array_push($availabs, $empAvail);
            }

        }

        return $availabs;

    }


    /**
     * Function parses availability according to given occurrences and returns only availability's parts that does not collide with any of given occurrences.
     * @param Collection $empOccurrs Occurrences that this employee has while he is available during his work.
     * @param $empAvail - Whole availability of an employee with start and end times.
     * @param array $availabs Already existing availabilities of employees where we should add next availabilities.
     * @param $duration Event's duration.
     * @return array Array of availabilities when there are no occurrences in that time.
     */
    private function getEmployeeAvails(Collection $empOccurrs, $empAvail, array $availabs, $duration)
    {
        //get the start of availability
        $availStart = Carbon::parse($empAvail->start_time);

        //for every occurrence of this employee
        for ($i = 0; $i < count($empOccurrs); $i++)
        {
            //get the end time of availability
            $availEnd = Carbon::parse($empAvail->end_time);

            //and get the start and end time of this occurrence
            $occurrStrt = Carbon::parse($empOccurrs[$i]->start_time);
            $occurrEnd = Carbon::parse($empOccurrs[$i]->end_time);


            // [     -----   ] ||  [----    ] => [] availability, -- occurrence
            if ($occurrStrt->between($availStart, $availEnd) && $occurrEnd->between($availStart, $availEnd))
            {
                //then we have to clone this availability
                $empAvailPart = clone $empAvail;

                //and set its end to occurrence start
                $empAvailPart->end_time = $occurrStrt->toDateTimeString();

                if ($this->checkIfFits(Carbon::parse($empAvailPart->start_time), Carbon::parse($empAvailPart->end_time), $duration))
                {   //if event can fit inside this time interval we can add first part of availability
                    array_push($availabs, $empAvailPart);
                }

                //and we adjust second part's start to occurrence end
                $empAvail->start_time = $occurrEnd->toDateTimeString();
                //and set whole availability's start time to the end of occurrence for next iteration
                $availStart = Carbon::parse($empAvail->start_time);

            } // ----[---    ] =>  [] availability, -- occurrence
            else if ($occurrStrt->lt($availStart) && $occurrEnd->between($availStart, $availEnd))
            {
                //we adjust availability start time to the end of occurrence for the next iteration
                $empAvail->start_time = $occurrEnd->toDateTimeString();
            }

            // if we are checking occurrence that precedes last one, we also check it the last one is long enought
            // to fit new occurrence inside it
            if ($i == count($empOccurrs) - 1
                &&
                $this->checkIfFits($availStart, Carbon::parse($empAvail->end_time), $duration))
            {   //if yes we add it to availabilities
                array_push($availabs, $empAvail);
            }

        }

        return $availabs;
    }

    /**
     * Function compares two times and returns true if difference between them is at least of duration size
     * @param Carbon $starTime first compared dateTime, it should be less than the second one
     * @param Carbon $endTime second compared dateTime, it should be more than the first one
     * @param $duration says how much should be these times different from each other
     * @return bool true if difference between times is at least of size duration
     */
    private function checkIfFits(Carbon $starTime, Carbon $endTime, $duration)
    {
        return ($starTime->diffInMinutes($endTime) >= $duration) && $endTime->gt($starTime);
    }

    private function checkAvailability(User $user, Carbon $startTime, Carbon $endTime, Event $event, $placeId)
    {


        $avlb = $user->availabilities()
            ->whereRaw('? between start_time and end_time and ? between start_time and end_time', [$startTime, $endTime])
            ->get();


        if (count($avlb) == 0)
        {
            return false;
        }

        $startTime = $startTime->copy()->addSecond();
        $endTime = $endTime->copy()->subSecond();

        $occrrs = $user->organiseOccurrences()
            ->where(function ($query) use ($startTime, $endTime)
            {
                $query->whereBetween('start_time', [$startTime, $endTime])
                    ->orWhereBetween('end_time', [$startTime, $endTime]);
            })
            ->get();

        $occps = \App\Place::find($placeId)->occupancies()
            ->where(function ($query) use ($startTime, $endTime)
            {
                $query->whereBetween('start_time', [$startTime, $endTime])
                    ->orWhereBetween('end_time', [$startTime, $endTime]);
            })
            ->get();

        if (count($occrrs) || count($occps))
        {
            return false;
        }

        return true;

    }

}
