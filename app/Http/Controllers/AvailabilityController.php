<?php

namespace App\Http\Controllers;

use App\Availability;
use App\Classes\Calendar;
use App\Place;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AvailabilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('/availability/index', compact('users'));
    }

    /**
     * Show the form for creating a new availability.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user)
    {
        $startTime = null;
        if (\request('start'))
        {
            $startTime = \request('start');
            $startTime = \Carbon\Carbon::parse($startTime);
        }
        $title = 'Vytvoriť Dostupnosť';
        $action = url('/availability/create');
        $calendar = Calendar::createSmall();
        return view('/availability/create', compact(['user', 'title', 'action', 'startTime', 'calendar']));
    }

    /**
     * Store a newly created availability in database.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dates = json_decode(\request('availabs'));

        $uId = \request('userId');
        //array for availabilities that cannot be stored
        $skipped = [];
        //array of availabilities that was successfully stored in DB
        $created =[];
        //array of availabilities that was not stored because of some reason, in most cases just empty array
        $createdNot = [];

        $isSetDate = \request('date');


        if (isset($isSetDate)){
            $dates[] = $isSetDate;
        }


        foreach ($dates as $date)
        {
            $day = Carbon::parse($date);

            $startTime = $day->copy()->setTimeFromTimeString(\request('startTime'));
            $endTime = $day->copy()->setTimeFromTimeString(\request('endTime'));

            $closingHours = DB::table('opening_hrs')->where('id', $day->dayOfWeek)->get();
            $closingHours = $day->setTimeFromTimeString($closingHours[0]->end_time);

            if ($endTime->gt($closingHours))
            {
                $endTime = $closingHours->copy();
            }

            if ($startTime->gte($endTime))
            {
                $endTime = $startTime->copy()->addHour();
            }


            $avlbs = User::findOrFail($uId)->availabilities()->where(function ($query) use ($startTime, $endTime)
            {
                $query->whereRaw('? between start_time and end_time or ? between start_time and end_time', [$startTime, $endTime]);
            })->get();

            if (count($avlbs))
            {
                $skipped[] = $day->format('d.m.Y');
            }
            else
            {
                $avlb = Availability::create([
                    'user_id' => $uId,
                    'start_time' => $startTime,
                    'end_time' => $endTime
                ]);
                if ($avlb->save())
                {
                    $created[] = $startTime->format('d.m.Y');
                }
                else
                {
                    $createdNot[] = $startTime->format('d.m.Y');
                }
            }
        }

        if (count($skipped))
        {
            session()->flash('warning', 'Čas pre dátumy: ' . implode(', ', $skipped) . ' koliduje s už existujúcou dostupnosťou a preto bol/i vynechaný/é.');
        }
        if (count($createdNot))
        {
            session()->flash('danger', 'Dostupnosti pre dátumy: ' . implode(', ', $createdNot) . ' sa nepodarilo vytvoriť.');
        }
        if (count($created))
        {
            session()->flash('message', 'Dostupnosti pre dátumy: ' . implode(', ', $created) . ' boli úspešne vytvorené.');
        }

        return redirect('availability/show?user=' . $uId);
    }

    /**
     * Display all availabilities along with corresponding occurrences for specified user or for all users.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $id = \request('user');
        $calendar = null;
        if ($id == 0)
        {
            //if user is not specified, show availabilities and occurrences of all users
            $calendar = $this->showAll();
        }
        else
        {
            $user = User::find($id);

            $availabs = $user->availabilities;

            $occurrs = $user->organiseOccurrences;
            //variable for all the things(availabilities/occurrences) we want to show inside our calendar
            $items = [];

            if ($availabs->count())
            {
                foreach ($availabs as $availab)
                {
                    $items[] = \Calendar::event(
                        $user->name,
                        false,
                        $availab->start_time,
                        $availab->end_time,
                        $availab->id,
                        [
                            'color' => '#85b1f7',
                            'clickable' => true,
                            'oldColor' => null
                        ]
                    );
                }

            }
            if ($occurrs->count())
            {
                foreach ($occurrs as $occurr)
                {
                    $color = $occurr->confirmed ? "#71d662" : '#d66666';

                    $items[] = \Calendar::event(
                        $occurr->event->title,
                        false,
                        $occurr->start_time,
                        $occurr->end_time,
                        null,
                        [
                            'color' => $color,
                            'clickable' => false
                        ]
                    );
                }
            }

            //customizing options of calendar
            $calendar = Calendar::createwEvents($items);
            $options = $calendar->getOptions();
            $options['editable'] = false;

            $calendar->setOptions($options);
            //if day is clicked user will be redirected to form for creating new availability
            $url = url('/availability/create/' . $user->id);
            $calendar->setCallbacks([
                'dayClick' => 'function(date, jsEvent, view) {
                if(view.name === \'agendaWeek\')
                {
                    window.location = "' . $url . '?start=" + date.format();

                }
            }',
                'eventClick' => 'function (calEvent) {
                        if (calEvent.clickable && calEvent.oldColor === null) {
                            calEvent.oldColor = calEvent.color;
                            calEvent.color = "#f7f585";
                            calEvent.clicked = true;
                        } 
                        else if(calEvent.clickable && calEvent.oldColor !== null)
                        {
                            calEvent.color = calEvent.oldColor;
                            calEvent.oldColor = null;
                            calEvent.clicked = false;
                        }
                        $(this).css("backgroundColor", calEvent.color);
                    }'
            ]);
        }
        //value is used inside view to control it
        $adminAccess = true;
        return view('/availability/calendar', compact(['calendar', 'user', 'adminAccess']));
    }

    /**
     * Display all availabilities along with corresponding occurrences for all users.
     * @return mixed set calendar
     */
    private function showAll()
    {
        $userIds = DB::table('role_user')->whereIn('role_id', [1, 3])->pluck('user_id');
        $users = User::find($userIds);
        //variable for all the things(availabilities/occurrences) we want to show inside our calendar
        $items = [];
        //control variable for changing colors of occurrences and availabilities
        $startColor = 70;

        //every place has its color
        for ($i = Place::all()->count() - 1; $i >= 0; $i--)
        {
            $rgbArr = Calendar::ColorHSLToRGB((($i * 120) % 360) / 360, 0.8, 0.4);
            $borderColor[$i] = 'rgb(' . intval($rgbArr['r']) . ',' . intval($rgbArr['g']) . ',' . intval($rgbArr['b']) . ')';
        }

        foreach ($users as $user)
        {
            $user->availabilities()->where('start_time', '>', Carbon::now()->setTime(0, 0)->subWeeks(3))->get();

            $availabs = $user->availabilities;

            $occurrs = $user->organiseOccurrences;

            $startColor = ($startColor + 70) % 360;

            $color = Calendar::ColorHSLToRGB($startColor / 360, 0.5, 0.6);

            if ($availabs->count())
            {
                foreach ($availabs as $availab)
                {
                    $items[] = \Calendar::event(
                        $user->name,
                        false,
                        $availab->start_time,
                        $availab->end_time,
                        $availab->id,
                        [
                            'color' => "rgb(" . intval($color['r']) . "," . intval($color['g']) . "," . intval($color['b']) . ")",
                            'clickable' => true,
                            'oldColor' => null
                        ]
                    );
                }

            }
            if ($occurrs->count())
            {
                foreach ($occurrs as $occurr)
                {
                    $url = url('/occurrences/' . $occurr->id);
                    $borderColorId = empty($occurr->place_id) ? 0 : $occurr->place_id - 1;

                    $items[] = \Calendar::event(
                        $occurr->event->title,
                        false,
                        $occurr->start_time,
                        $occurr->end_time,
                        null,
                        // Add color and link on event
                        [
                            'color' => "rgb(" . intval($color['r']) . "," . intval($color['g']) . "," . intval($color['b']) . ")",
                            'borderColor' => $borderColor[$borderColorId],
                            'url' => $url,
                            'clickable' => false,
                            'className' => 'moreBorder'
                        ]
                    );
                }
            }

        }

        $calendar = Calendar::createwEvents($items);
        $options = $calendar->getOptions();
        $options['editable'] = false;

        $calendar->setOptions($options);

        $calendar->setCallbacks([
            'eventClick' => 'function (calEvent) {
                        if (calEvent.clickable && calEvent.oldColor === null) {
                            calEvent.oldColor = calEvent.color;
                            calEvent.color = "#f7f585";
                            calEvent.clicked = true;
                        } 
                        else if(calEvent.clickable && calEvent.oldColor !== null)
                        {
                            calEvent.color = calEvent.oldColor;
                            calEvent.oldColor = null;
                            calEvent.clicked = false;
                        }
                        $(this).css("backgroundColor", calEvent.color);
                    }'
        ]);

        return $calendar;
    }

    /**
     * Show the form for editing the specified Availability.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Availability $availab)
    {
        return view('/availability/edit', compact('availab'));
    }

    /**
     * Update the specified availability.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Availability $availab)
    {
        $occrrs = $availab->user->organiseOccurrences()
            ->whereBetween('start_time', [$availab->start_time, $availab->end_time])
            ->whereBetween('end_time', [$availab->start_time, $availab->end_time])
            ->get();

        $startTime = Carbon::parse($availab->start_time)->setTimeFromTimeString(\request('startTime'));
        $endTime = $startTime->copy()->setTimeFromTimeString(\request('endTime'));
        if ($startTime->gte($endTime))
        {
            $endTime = $startTime->copy()->addMinute();
        }

        if (count($occrrs))
        {
            $select = $occrrs->filter(
                function ($item) use ($startTime, $endTime)
                {
                    $itemStart = Carbon::parse($item->start_time);
                    $itemEnd = Carbon::parse($item->end_time);
                    return !($itemStart->between($startTime, $endTime) && $itemEnd->between($startTime, $endTime));
                })->all();

            if (count($select))
            {
                session()->flash('warning', 'V tomto čase sa konajú udalosti, nepodarilo sa upraviť túto dostupnosť.');
                $url = url('/availability/' . $availab->id . '/edit');
                return redirect($url);
            }


        }

        $availab->start_time = $startTime->toDateTimeString();
        $availab->end_time = $endTime->toDateTimeString();
        if ($availab->save())
        {
            session()->flash('message', 'Dostupnosť bola upravená.');
        }
        else
        {
            session()->flash('warning', 'Dostupnosť sa nepodarilo upraviť.');
        }
        $url = url('/availability/show?user=' . $availab->user_id);
        return redirect($url);
    }

    /**
     * Remove the specified availability from database.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Availability $availab)
    {
        $occrrs = $availab->user->organiseOccurrences()
            ->whereBetween('start_time', [$availab->start_time, $availab->end_time])
            ->whereBetween('end_time', [$availab->start_time, $availab->end_time])
            ->get();
//        dd($occrrs);

        if (count($occrrs))
        {
            session()->flash('warning', 'Dostupnosť nebola odstránená, pretože zamestnanec organizuje v tomto čase podujatia.');
            return redirect('/availability/show?user=' . $availab->user_id);
        }
        else
        {
            if ($availab->delete())
            {
                session()->flash('message', 'Dostupnosť bola odstránená.');
            }
            else
            {
                session()->flash('warning', 'Dostupnosť sa nepodarilo odstrániť.');
            }
            return redirect('/availability/show?user=' . $availab->user_id);
        }
    }

    /**
     * Removes all specified availabilities from database.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroyMultiple()
    {
        $availabIds = \request()->all();

        if (count($availabIds))
        {
            $userId = Availability::find($availabIds[0])->user_id;

            foreach ($availabIds as $id)
            {
                DB::table('availabilities')->where('id', $id)->delete();
            }
        }
        else
        {
            $userId = auth()->user()->id;
        }

        return redirect('/availability/show?user=' . $userId);
    }


}
