<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () 
	{
		$cur_page = "home";
		return(view('index/index',compact("cur_page")));
	}
);

// EventsController
Route::get('/events', 'EventsController@index');

Route::get('/calendar', 'CalendarController@index');

// EventsController
Route::get('/events/create', 'EventsController@create')->middleware('employee');
Route::post('/events/create', 'EventsController@store')->middleware('employee');
Route::get('/events/creator', 'EventsController@creator')->middleware('employee');
Route::patch('/events/creator', 'EventsController@updateCreator')->middleware('employee');
Route::get('/events/{event}', 'EventsController@show');
Route::get('/events/{event}/edit', 'EventsController@edit')->middleware('employee','creator');
Route::get('/events/{event}/delete', 'EventsController@destroy')->middleware('creator');
Route::patch('/events/{event}', 'EventsController@update')->middleware('employee','creator');

// TgroupsController
Route::get('/tgroups/create', 'TgroupsController@create')->middleware('employee');
Route::post('/tgroups/create', 'TgroupsController@store')->middleware('employee');
Route::get('/tgroups/index', 'TgroupsController@index')->middleware('employee');
Route::post('/tgroups/destroy', 'TgroupsController@destroy')->middleware('employee');

// PlacesController
Route::get('/places/create', 'PlacesController@create')->middleware('employee');
Route::post('/places/create', 'PlacesController@store')->middleware('employee');
Route::get('/places/index', 'PlacesController@index')->middleware('employee');
Route::post('/places/destroy', 'PlacesController@destroy')->middleware('employee');

// OccurrenceController
Route::get('/occurrences/selectEvent', 'OccurrenceController@index')->middleware('auth');
Route::get('/occurrences/selectNoEvent', 'OccurrenceController@indexNoEvent')->middleware('auth');
Route::post('/occurrences/reservation', 'OccurrenceController@reservation')->middleware('auth');
Route::post('/occurrences/reserve', 'OccurrenceController@create')->middleware('auth');
Route::post('/occurrences/create', 'OccurrenceController@store')->middleware('auth');
Route::get('/occurrences/{availab}/createBlock', 'OccurrenceController@createBlock')->middleware('employee');
Route::post('/occurrences/createBlock', 'OccurrenceController@storeBlock')->middleware('employee');
Route::post('/occurrences/reserveNoEvent', 'OccurrenceController@reservationNoEvent')->middleware('auth');
Route::get('/occurrences/{occrr}/cancel', 'OccurrenceController@destroyMessage')->middleware('auth');
Route::post('/occurrences/{occrr}/cancel', 'OccurrenceController@destroy')->middleware('auth');
Route::get('/occurrences/{occrr}/confirm', 'OccurrenceController@confirm')->middleware('employee');
Route::post('/occurrences/{occrr}/confirm', 'OccurrenceController@confirmStore')->middleware('employee');
Route::get('/occurrences/{occrr}', 'OccurrenceController@show')->middleware('employee');
Route::get('/occurrences/{occrr}/attendee', 'OccurrenceController@showAttendee')->middleware('auth');

// UsersController
Route::get('/users/edit', 'UsersController@edit')->middleware('admin');
Route::post('/users/update', 'UsersController@update')->middleware('admin');
Route::get('/users/{user}/delete', 'UsersController@destroy')->middleware('admin');
Route::get('/users/availabs', 'UsersController@index')->middleware('employee');
Route::get('/users/{availab}/availab', 'UsersController@show')->middleware('employee');
Route::get('/users/{user}/contact', 'UsersController@setContactPerson')->middleware('admin');

// AvailabilityController
Route::get('/availability/index', 'AvailabilityController@index')->middleware('admin');
Route::get('/availability/show', 'AvailabilityController@show')->middleware('admin');
Route::get('/availability/{availab}/edit', 'AvailabilityController@edit')->middleware('admin');
Route::patch('/availability/{availab}/update', 'AvailabilityController@update')->middleware('admin');
Route::get('/availability/{availab}/delete', 'AvailabilityController@destroy')->middleware('admin');
Route::get('/availability/delete', 'AvailabilityController@destroyMultiple')->middleware('admin');
Route::get('/availability/create/{user}', 'AvailabilityController@create')->middleware('admin');
Route::post('/availability/create', 'AvailabilityController@store')->middleware('admin');

Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home', 'EventsController@index')->name('home');
