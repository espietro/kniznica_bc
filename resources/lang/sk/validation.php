<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':Attribute musí byť prijatý.',
    'active_url'           => ':Attribute nie je platný URL',
    'after'                => ':Attribute musí byť dátum nasledujúci po dátume :date.',
    'after_or_equal'       => ':Attribute musí byť dátum nasledujúci po alebo rovný dátumu :date.',
    'alpha'                => ':Attribute môže obsahovať iba písmená.',
    'alpha_dash'           => ':Attribute môže obsahovať iba písmená, čísla a pomlčky.',
    'alpha_num'            => ':Attribute môže obsahovať iba písmená a čísla.',
    'array'                => ':Attribute musí byť pole.',
    'before'               => ':Attribute musí byť dátum pred dátumom :date.',
    'before_or_equal'      => ':Attribute musí byť dátum predchádzajúci alebo rovný dátumu :date.',
    'between'              => [
        'numeric' => ':Attribute musí byť v rozmedzí :min a :max.',
        'file'    => ':Attribute musí byť v rozmedzí :min a :max kilobyty/ov.',
        'string'  => ':Attribute musí byť v rozmedzí :min a :max znaky/ov.',
        'array'   => 'Počet prvkov v poli :attribute musí byť v rozmedzí :min až :max.',
    ],
    'boolean'              => 'Pole :attribute musí byť pravda alebo nepravda.',
    'confirmed'            => ':Attribute sa nezhoduje.',
    'date'                 => 'Dátum :attribute nie je platným dátumom.',
    'date_format'          => 'Dátum :attribute nevyhovuje formátu :format.',
    'different'            => ':Attribute a :other sa musia líšiť.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'dimensions'           => 'The :attribute has invalid image dimensions.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => ':Attribute musí byť platná emailová adresa.',
    'exists'               => ':Attribute nie je platný.',
    'file'                 => ':Attribute musí byť súbor.',
    'filled'               => 'Pole :attribute musí mať hodnotu.',
    'image'                => ':Attribute musí byť obrázok.',
    'in'                   => ':Attribute nie je platný.',
    'in_array'             => 'Pole :Attribute sa nenachádza v :other.',
    'integer'              => ':Attribute musí byť číslo.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'ipv4'                 => 'The :attribute must be a valid IPv4 address.',
    'ipv6'                 => 'The :attribute must be a valid IPv6 address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => ':Attribute nesmie byť viac ako :max.',
        'file'    => ':Attribute nesmie mať viac ako :max kilobyty/ov.',
        'string'  => ':Attribute nesmie obsahovať viac ako :max znaky/ov.',
        'array'   => 'Pole :attribute nesmie obsahovať viac ako :max prvky/ov.',
    ],
    'mimes'                => ':Attribute musí byť súbor typu: :values.',
    'mimetypes'            => ':Attribute musí byť súbor typu: :values.',
    'min'                  => [
        'numeric' => ':Attribute musí byť väčší/rovný :min.',
        'file'    => 'Súbor :attribute musí mať aspoň :min kilobyty/ov.',
        'string'  => 'Reťazec :attribute musí mať aspoň :min znaky/ov.',
        'array'   => 'Pole :attribute musí mať aspoň :min prvkov.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => ':attribute musí byť číslo.',
    'present'              => 'The :attribute field must be present.',
    'regex'                => ':Attribute má nesprávny formát.',
    'required'             => 'Pole :attribute je povinné.',
    'required_if'          => 'Pole :attribute je povinné ak :other má hodnotu :value.',
    'required_unless'      => 'Pole :attribute je povinné pokiaľ pole :other nemá hodnotu/y :values.',
    'required_with'        => 'Pole :attribute je povinné ak pole :values je zadané.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'Pole :attribute a :other musia byť zhodné.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => ':Attribute musí byť reťazec.',
    'timezone'             => ':Attribute musí byť platná časová zóna.',
    'unique'               => ':Attribute už existuje.',
    'uploaded'             => ':Attribute sa nepodarilo nahrať.',
    'url'                  => ':Attribute nemá platný formát.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'phone' => 'tel. číslo',
        'password' => 'heslo'
    ],

];
