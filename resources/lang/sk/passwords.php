<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Heslo musí byť aspoň šesť znakov dlhé a heslá sa musia zhodovať.',
    'reset' => 'Vaše heslo bolo obnovené!',
    'sent' => 'Na Váš email sme odoslali link pre obnovenie hesla!',
    'token' => 'Token pre obnovu hesla nie je platný.',
    'user' => "Nevieme nájsť používateľa s touto e-mailovou adresou.",

];
