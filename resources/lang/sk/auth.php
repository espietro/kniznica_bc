<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Tieto údaje nezodpovedajú informáciam uloženým v systéme.',
    'throttle' => 'Príliš veľa pokusov o prihlásenie. Skúste to o :seconds sekúnd prosím.',

];
