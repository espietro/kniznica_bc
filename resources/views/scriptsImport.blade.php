    {{-- Bootstrap core JavaScript
    ================================================== --}}
    {{-- Placed at the end of the document so the pages load faster --}}
{{--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>--}}
    {{--<script src="{{ asset('js/jquery-3.2.1.slim.min.js') }}"></script>--}}
    <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
{{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>--}}
    <script src="{{ asset('js/bootstrap-4.0.0.min.js') }}"></script>

@if(isset($calendar))
    {{--<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>--}}
    <script src="{{ asset('js/moment-2.9.0.min.js') }}"></script>

    {{--<script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>--}}
    <script src="{{ asset('js/fullcalendar-2.2.7.min.js') }}"></script>

    {{--<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>--}}
    <link rel="stylesheet" href="{{ asset('css/fullcalendar-2.2.7.min.css') }}"/>

    {!! $calendar->script() !!}


@endif