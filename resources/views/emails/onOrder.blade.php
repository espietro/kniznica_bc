<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Rezervácia</title>
</head>
<body>

    <h3>Dobrý deň {{ $recipient->name }},</h3>
    <p>
        Vaša rezervácia, na podujatie {{ $occrr->event->title }}, bola zaregistrovaná,
        prosím očakávajte potvrdzovací email, ktorý Vám pošle pracovník knižnice.
    </p>

    <p>
        Detaily rezervácie:
    </p>

    <p>
        <b>Názov:</b> {{ $occrr->event->title }} <br/>
        <b>Čas začiatku:</b> {{ \Carbon\Carbon::parse($occrr->start_time)->format('H:i - d.m.Y') }}<br/>
        <b>Čas konca:</b> {{ \Carbon\Carbon::parse($occrr->end_time)->format('H:i - d.m.Y') }}<br/>

        <b>Popis:</b> {{ $occrr->event->subject }} <br/>

    </p>

</body>
</html>