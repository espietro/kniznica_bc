@extends('events/layout')


@section('my_title')
    {{ $title }}
@endsection


@section('content')

    <div class="container col-md-8 jumbotron">

        <h1 class="jumbotron-heading">Správa pre užívateľa {{ auth()->user()->isEmployee() ? $occrr->user->name :  $occrr->organiser->name}}</h1>
        <p >
            Udalosť <span class="text-info">{{ $occrr->event->title }}</span><br/>
            Začiatok: {{ \Carbon\Carbon::parse($occrr->start_time)->format('H:i d.m.Y') }}<br/>
            Koniec: {{ \Carbon\Carbon::parse($occrr->end_time)->format('H:i d.m.Y') }}<br/>
        </p>
        <hr/>

        <form method="POST" action="{{ url($action) }}" id="eventForm" enctype="multipart/form-data">
            {{ csrf_field() }}

           <div class="form-group">
                <label for="message">Správa:</label>
                <textarea id="message" name="message" class="form-control" rows="4" cols="50">
    {{ $myMessage }}
                </textarea>
            </div>
            @if ($errors->has('message'))
                <span class="help-block">
                    <strong>{{ $errors->first('message') }}</strong>
                </span>
            @endif

            <div>
                <button type="submit" class="btn btn-primary">Odoslať</button>
            </div>
        </form>
    </div>

@endsection
