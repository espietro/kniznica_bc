<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Potvrdenie</title>
</head>
<body>

<h3>Dobrý deň {{ $occrr->user->name }},</h3>
<p>
    @if(empty($myMessage))
    Vaša rezervácia, na podujatie {{ $occrr->event->title }}, bola <b>potvrdená</b>.
    Tešíme sa na Vás.
    @else
        {{ $myMessage }}
    @endif
</p>

<p>
    Detaily Udalosti:
</p>

<p>
    <b>Názov:</b> {{ $occrr->event->title }} <br/>
    <b>Čas začiatku:</b> {{ \Carbon\Carbon::parse($occrr->start_time)->format('H:i - d.m.Y') }}<br/>
    <b>Čas konca:</b> {{ \Carbon\Carbon::parse($occrr->end_time)->format('H:i - d.m.Y') }}<br/>
    <b>Miestnosť:</b> {{ $occrr->place->name }}<br/>
    <b>Popis:</b> {{ $occrr->event->subject }} <br/>

</p>

</body>
</html>