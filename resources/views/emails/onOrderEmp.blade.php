<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Rezervácia</title>
</head>
<body>

<h3>Dobrý deň {{ $recipient->name }},</h3>
<p>
    <b>Užívateľ</b> <br/><br/>

    <b>meno:</b>{{ $sender->name }}<br/>
    <b>email:</b> {{ $sender->email }}<br/>
    <b>tel:</b> {{ $sender->phone }}<br/>
    <b>inštitúcia:</b> {{ $sender->institution }}<br/><br/>

    </b>vytvoril rezerváciu</b> <br/><br/>

    <b>Názov:</b> {{ $occrr->event->title }} <br/>
    <b>Čas začiatku:</b> {{ \Carbon\Carbon::parse($occrr->start_time)->format('H:i - d.m.Y') }}<br/>
    <b>Čas konca:</b> {{ \Carbon\Carbon::parse($occrr->end_time)->format('H:i - d.m.Y') }}<br/>
    <b>Miestnosť:</b> {{ $occrr->place->name }}<br/>
    <b>Popis:</b> {{ $occrr->event->subject }} <br/>
</p>

</body>
</html>