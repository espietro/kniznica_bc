@extends('events/layout')

@section('my_title')
    Pridať Miestnosť
@endsection


@section('content')

    <div class="container col-md-8 jumbotron">

        <h1 class="jumbotron-heading">Pridať Novú Miestnosť</h1>
        <hr/>
        <form method="POST" action="{{ url('/places/create') }}" id="placeForm" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="place">Názov/Meno:</label>
                <input type="text" name="place" id="place" class="form-control" required/>
            </div>
            @if ($errors->has('place'))
                <span class="help-block">
                <strong>{{ $errors->first('place') }}</strong>
            </span>
            @endif


            <button type="submit" class="btn btn-primary">Pridať</button>

        </form>
    </div>

@endsection
