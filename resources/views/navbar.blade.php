<header>
    <nav class="navbar navbar-expand-md navbar-dark static-top bg-dark">
        <a class="navbar-brand" href="">Logo</a>

        {{-- <span class="col-sm-1"></span> --}}
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=" #navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        {{-- menu list inside navigation panel --}}
        <div class="collapse navbar-collapse" id="navbarHeader">
            <ul class="navbar-nav mr-auto">

                <li class="nav-item @if (!empty($cur_page) && $cur_page == 'home') 
                                        active 
                                    @endif">
                    <a class="nav-link" href="@if(\Auth::check()) {{ url('/events') }} @else {{ url('/') }} @endif">
                        Domov 
                        <span class="sr-only">
                            (current)
                        </span>
                    </a>
                </li>
                <li class="nav-item @if (!empty($cur_page) && $cur_page == 'events') 
                                        active 
                                    @endif">
                    <a class="nav-link" href="{{ url('/events') }}">
                        Podujatia
                    </a>
                </li>
                <li class="nav-item @if (!empty($cur_page) && $cur_page == 'calendar') 
                                        active 
                                    @endif">
                    <a class="nav-link" href="{{ url('/calendar') }}">
                        Kalendar
                    </a>
                </li>

            </ul>
            <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li class="pl-2 pr-2"><a id="login" href="{{ route('login') }}">Prihlásiť</a></li>
                            <li class="pl-2 pr-2"><a href="{{ route('register') }}">Registrovať</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle pr-3" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}" class="pl-2"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Odhlásiť sa
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>

              {{-- formular for search --}}
            <form class="form-inline mt-2 mt-md-0">
                <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">
                    Search
                </button>
            </form>

        </div>
    </nav>
</header>

