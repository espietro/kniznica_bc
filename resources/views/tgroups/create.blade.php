@extends('events/layout')

@section('my_title')
    Vytvoriť Cieľovú Skupinu
@endsection


@section('content')
    
<div class="container col-md-8 jumbotron">

    <h1 class="jumbotron-heading">Nová Cieľová Skupina</h1>
    <hr/>
    <form method="POST" action="{{ url('/tgroups/create') }}" id="tgroupForm" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="form-group">
            <label for="tgroup">Názov/Meno:</label>
            <input type="text" name="tgroup" id="tgroup" class="form-control" required/>
        </div>
        @if ($errors->has('tgroup'))
            <span class="help-block">
                <strong>{{ $errors->first('tgroup') }}</strong>
            </span>
        @endif


        <button type="submit" class="btn btn-primary">Pridať</button>
        
    </form>
</div>

@endsection
