@extends('events/layout')


@section('my_title')
    Výber podujatia
@endsection


@section('content')

    <div class="container col-md-8 jumbotron">
        <h1 class="jumbotron-heading">Prihlásiť sa na podujatie</h1>
        <hr/>

        <form method="POST" action="{{ url('/occurrences/reserveNoEvent') }}" id="occurrenceForm2" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="tgroups" class="form-label">Cieľové skupiny</label>
                <select multiple id="tgroups" name="tgroups[]" class="form-control" required
                        {{--oninvalid="this.setCustomValidity('Prosím vyberte aspoň jednu cieľovú skupinu')"--}}
                >
                    @foreach($tgroups as $tgroup)
                        <option value="{{ $tgroup->id }}">{{ $tgroup->name }}</option>
                    @endforeach
                </select>
            </div>


            <div class="form-group">
                <label for="startDay" class="form-label">Pre dátum:</label>
                <input class="form-control" type="date" value="{{ \Carbon\Carbon::tomorrow()->toDateString() }}"
                       id="startDay" name="startDay"
                       min="{{ \Carbon\Carbon::now()->toDateString() }}"
                       {{--oninvalid="this.setCustomValidity('Dátum musí byť minimálne {{ \Carbon\Carbon::now()->format('d.m.Y') }}')"--}}
                />
            </div>
            @if ($errors->has('startDay'))
                <span class="help-block">
                        <strong>{{ $errors->first('startDay') }}</strong>
                    </span>
            @endif


            <button type="submit" class="btn btn-primary btn-md">Vyhľadaj termíny</button>
        </form>

    </div>

@endsection
