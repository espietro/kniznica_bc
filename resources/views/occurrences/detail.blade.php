@extends('events/layout')


@section('my_title')
    {{ $occrr->event->title }}
@endsection



@section('content')
    <div class="container">
        <h1 class="mt-5">{{ $occrr->event->title }}</h1>

        <hr/>

        @if(Auth::check() && (\Auth::user()->isOrganiser($occrr->id) || \Auth::user()->isAttendee($occrr->id)))

                <a type="button" href="{{ url('/occurrences/' . $occrr->id . '/cancel') }}"
                   class="float-right btn btn-outline-danger btn-sm">Zrusit</a>
                   
            @if(!$occrr->confirmed && \Auth::user()->isOrganiser($occrr->id))
                <a type="button" href="{{ url('/occurrences/' . $occrr->id . '/confirm') }}"
                   class="float-right btn btn-outline-primary btn-sm">Potvrdiť</a>
            @endif
        @endif

        <b>Miestnosť:</b>&nbsp;{{ $occrr->place->name }}<br/>
        <b>Čas od-do:</b>&nbsp;
        <p>
            {{ \Carbon\Carbon::parse($occrr->start_time)->format('H:i') }} -
            {{ \Carbon\Carbon::parse($occrr->end_time)->format('H:i &\nb\sp;&\nb\sp; d.m.Y') }}
        </p>

        @if(\Auth::check() && (\Auth::user()->isEmployee() || auth()->user()->isAttendee($occrr->id)))
            @if(\Auth::user()->isEmployee())
                <b>ID:</b>&nbsp;{{ $occrr->id }}<br/>
            @endif



            <b>Trvanie:</b>&nbsp;{{ $occrr->event->duration }} minút<br/>
            <b>Organizátor/prednášajúci:</b>
                <p>
                    <span>Meno: </span>{{ $occrr->organiser->name }}<br/>
                    <span>Telefon: </span>{{ $occrr->organiser->phone }}<br/>
                    <span>Mail: </span> {{ $occrr->organiser->email }}<br/>
                </p>

            <b>Účastník:</b>
            <p>
                @if($occrr->user != null)
                    <span>Meno: </span>{{ $occrr->user->name }}<br/>
                    <span>Telefon: </span>{{ $occrr->user->phone }}<br/>
                    <span>Mail: </span> {{ $occrr->user->email }}<br/>
                @else
                    {{ '(Užívateľ nebol prihlásený)' }}
                @endif
            </p>
        @endif

        <b>Popis:</b>&nbsp;<p>{{ $occrr->event->subject }}<br/></p>
        @if(sizeof($occrr->event->tgroups)==1)
            <b>Cielova skupina:</b>&nbsp;
        @else
            <b>Cielove skupiny:</b>&nbsp;
        @endif
        <ul>
            @foreach($occrr->event->tgroups as $tgroup)
                <li>{{ $tgroup->name }}</li>
            @endforeach
        </ul>
        <hr/>
    </div>
@endsection
