@extends('events/layout')


@section('my_title')
    Vytvoriť Rezerváciu
@endsection


@section('content')



    <div class="container">

        @if(count($intrvls)>0)
            <h1 class="mt-5">Dostupné časové intervaly, kedy je možné sa na podujatie prihlásiť:</h1>
            <h5 class="text-info">Po výbere budete môcť zadať čas začiatku tak, ako vám to vyhovuje, v danom
                časovom rozmedzí.</h5>
            <h4 class="text-primary">Podujatie: {{ \App\Event::find($eventId)->title }}</h4>
{{--            {!! ""; $i = 1; !!}--}}
{{--            @foreach($intrvls as $intrvl)--}}
            @for ($i = 0; $i < count($intrvls); $i++)
                @if($i == 0 || !\Carbon\Carbon::parse($intrvls[$i]->startTime)->isSameDay(\Carbon\Carbon::parse($intrvls[$i-1]->startTime)))
                     <h5 class="text-primary text-center mt-5">{{ \Carbon\Carbon::parse($intrvls[$i]->startTime)->format('d.m.Y') }}</h5>
                @endif
                <div class="row ">
                    <div class="card col m-0">

                        <div class="card-header p-2 row">
                            <span class="col">Od:</span>

                            <span class="col">Do:</span>

                            <span class="col">Dátum:</span>

                            @if(auth()->check() && auth()->user()->isEmployee())
                                <span class="col">Miestnosť:</span>
                            @endif
                        </div>

                        <div class="card-body bg-light p-2 row">

                            <span class="col">{{ \Carbon\Carbon::parse($intrvls[$i]->startTime)->format('H:i') }}</span>

                            <span class="col">{{ \Carbon\Carbon::parse($intrvls[$i]->endTime)->format('H:i') }}</span>

                            <span class="col">{{ \Carbon\Carbon::parse($intrvls[$i]->endTime)->format('d.m.Y') }}</span>

                            @if(auth()->check() && auth()->user()->isEmployee())
                                <span class="col">{{ App\Place::find($intrvls[$i]->placeId)->name }}</span>
                            @endif

                        </div>

                        <div class="card-footer p-2 row">
                            @if(auth()->check() && auth()->user()->isEmployee())
                                <h6 class="col"> {{ App\User::find($intrvls[$i]->userId)->name }}</h6>
                            @endif
                            <form method="POST" action="{{ url('/occurrences/reserve') }}" id="occurenceForm"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="startTime" id="startTime" value="{{ $intrvls[$i]->startTime }}">
                                <input type="hidden" name="endTime" id="endTime" value="{{ $intrvls[$i]->endTime }}">
                                <input type="hidden" name="placeId" id="placeId" value="{{ $intrvls[$i]->placeId }}">
                                <input type="hidden" name="userId" id="userId" value="{{ $intrvls[$i]->userId }}">
                                <input type="hidden" name="eventId" id="eventId" value="{{ $eventId }}">
                                <button class="btn btn-primary btn-sm" type="submit">Vybrať</button>
                            </form>
                        </div>

                    </div>

                </div>

            {{--@endforeach--}}

            @endfor
        @else
            <p class="text-info text-center jumbotron">Pre zadané hodnoty neboli nájdené žiadne výsledky.</p>

        @endif
    </div>
@endsection
