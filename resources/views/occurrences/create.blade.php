@extends('events/layout')


@section('my_title')
    Prihlásiť Na Podujatie
@endsection


@section('content')

    <div class="container col-md-8 jumbotron">

        <h1 class="jumbotron-heading">Prihlásiť sa na podujatie: </h1>
        <h3>{{ $event->title }}</h3>
        <h5>{{ \Carbon\Carbon::parse($interval->startTime)->format('d.m.Y') }}</h5>
        <p class="text-info">Podujatie na ktoré sa chcete prihlásiť má trvanie {{ $event->duration }} minút.<br/>
            Zadajte prosím čas začiatku podujatia v rozmedzí
            od {{ \Carbon\Carbon::parse($interval->startTime)->format('H:i') }}
            - {{ \Carbon\Carbon::parse($interval->endTime)->subMinutes($event->duration)->format('H:i') }} hod.
        </p>
        <hr/>

        <form method="POST" action="{{ url('/occurrences/create') }}" id="occurrenceForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{--<h5>Chcem aby sa podujatie začalo v čase:</h5>--}}

            <div class="form-group">
                <label for="startTime">Chcem aby sa podujatie začalo v čase:</label>
                <input type="time" name="startTime" id="startTime" class="form-control"
                       min="{{ \Carbon\Carbon::parse($interval->startTime)->format('H:i') }}"
                       max="{{ \Carbon\Carbon::parse($interval->endTime)->subMinutes($event->duration)->addMinute()->format('H:i') }}"
                       value="{{ \Carbon\Carbon::parse($interval->startTime)->format('H:i') }}" required
                       {{--oninvalid="this.setCustomValidity('Čas musí byť v rozmedzí {{ \Carbon\Carbon::parse($interval->startTime)->format('H:i') }} až {{ \Carbon\Carbon::parse($interval->endTime)->subMinutes($event->duration)->format('H:i') }}')"--}}
                />
            </div>
            @if ($errors->has('startTime'))
                <span class="help-block">
                    <strong>{{ $errors->first('startTime') }}</strong>
                </span>
            @endif


            <input type="hidden" name="date" id="date" value="{{ $interval->startTime }}">
            <input type="hidden" name="placeId" id="placeId" value="{{ $interval->placeId }}">
            <input type="hidden" name="eventId" id="eventId" value="{{ $event->id }}">
            <input type="hidden" name="userId" id="userId" value="{{ $interval->userId }}">


            <button type="submit" class="btn btn-primary">Potvrdiť</button>
        </form>
    </div>

@endsection
