@extends('events/layout')


@section('my_title')
    Upraviť Udalosť
@endsection


@section('content')

    <div class="container col-md-8 jumbotron">

        <h1 class="jumbotron-heading">Upraviť Udalosť</h1>
        <hr/>
        <form method="POST" action="{{ url('/occurrences/' . $occrr->id . '') }}" id="eventForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}

            {{--<div class="form-group">--}}
                {{--<label for="title">Názov:</label>--}}
                {{--<input type="text" name="title" id="title" class="form-control" value="{{ $event->title }}" required/>--}}
            {{--</div>--}}
            {{--@if ($errors->has('title'))--}}
                {{--<span class="help-block">--}}
                {{--<strong>{{ $errors->first('title') }}</strong>--}}
            {{--</span>--}}
            {{--@endif--}}

            {{--<div class="form-group">--}}
                {{--<label for="subject">Téma/Popis:</label>--}}
                {{--<textarea id="subject" name="subject" class="form-control" rows="4"--}}
                          {{--cols="50">{{ $event->subject }}</textarea>--}}
            {{--</div>--}}

            {{--@if ($errors->has('subject'))--}}
                {{--<span class="help-block">--}}
                {{--<strong>{{ $errors->first('subject') }}</strong>--}}
            {{--</span>--}}
            {{--@endif--}}

            {{--<div class="form-group">--}}
            {{--<label for="tgroup">Cieľová skupina:</label>--}}
            {{--@foreach($tgroups as $tgroup)--}}
            {{--<label>--}}
            {{--<input type="checkbox" id="tgroup{{ $tgroup->id }}" name="tgroup[]" value="{{ $tgroup->id }}"--}}
            {{--@foreach($event->tgroups as $egroup)--}}
            {{--@if($tgroup->name == $egroup->name)--}}
            {{--checked--}}
            {{--@break--}}
            {{--@endif--}}
            {{--@endforeach--}}
            {{-->--}}
            {{--&nbsp;{{ $tgroup->name }}--}}
            {{--</label>--}}
            {{--@endforeach--}}
            {{--<a href="/tgroups/create">Nová</a>--}}
            {{--</select>--}}
            {{--</div>--}}
            {{--@if ($errors->has('tgroup'))--}}
            {{--<span class="help-block">--}}
            {{--<strong>{{ $errors->first('tgroup') }}</strong>--}}
            {{--</span>--}}
            {{--@endif--}}


            <div class="form-group">
                <label for="place">Miestnosť:</label>

                <select id="place" name="place">

                    @foreach($occrr->event->places as $place)
                        <option value="{{ $place->id }}">{{ $place->name }}</option>

                    @endforeach


                </select>
            </div>
            @if ($errors->has('place'))
                <span class="help-block">
                    <strong>{{ $errors->first('place') }}</strong>
                </span>
            @endif


            {{--<div class="form-group">--}}
                {{--<label for="employee">Organiztor(i):</label>--}}
                {{--@foreach($employees as $employee)--}}
                    {{--@if($employee->id != $event->creator->id)--}}
                        {{--<label>--}}
                            {{--<input type="checkbox" id="employee{{ $employee->id }}" name="employee[]"--}}
                                   {{--value="{{ $employee->id }}"--}}
                                   {{--@foreach($event->users as $euser)--}}
                                   {{--@if($employee->id == $euser->id)--}}
                                   {{--checked--}}
                                    {{--@break--}}
                                    {{--@endif--}}
                                    {{--@endforeach--}}
                            {{-->--}}
                            {{--&nbsp;{{ $employee->name }}--}}
                        {{--</label>--}}
                    {{--@endif--}}
                {{--@endforeach--}}
                {{--<a href="/employees/create">Nový</a>--}}
                {{--</select>--}}
            {{--</div>--}}
            {{--@if ($errors->has('employee'))--}}
                {{--<span class="help-block">--}}
                    {{--<strong>{{ $errors->first('employee') }}</strong>--}}
                {{--</span>--}}
            {{--@endif--}}

            <button type="submit" class="btn btn-primary">Potvrdiť</button>

        </form>
    </div>

@endsection
