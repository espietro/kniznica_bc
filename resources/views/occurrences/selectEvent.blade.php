@extends('events/layout')


@section('my_title')
    Výber podujatia
@endsection


@section('content')

    <div class="container col-md-8 jumbotron">
        <h1 class="jumbotron-heading">Prihlásiť sa na podujatie</h1>
        <hr/>

        <form class="mb-5" method="POST" action="{{ url('/occurrences/reservation') }}" id="occurrenceForm" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="event">Ktoré podujatie?</label>
                <select class="form-control" id="event" name="event">
                    @foreach($events as $event)
                        @if($event->visible)
                            <option value="{{ $event->id }}">{{ $event->title }}</option>
                        @endif
                    @endforeach
                    {{--<option value="0">Prázdny Blok (žiadne podujatie)</option>--}}
                </select>
            </div>
            @if ($errors->has('event'))
                <span class="help-block">
                    <strong>{{ $errors->first('event') }}</strong>
                </span>
            @endif


            <div class="form-group">
                <label for="startDay" class="form-label">Od:</label>
                <input class="form-control" type="date" value="{{ \Carbon\Carbon::tomorrow()->toDateString() }}"
                       id="startDay" name="startDay"
                       min="{{ \Carbon\Carbon::now()->toDateString() }}"
                       {{--oninvalid="this.setCustomValidity('Dátum musí byť minimálne {{ \Carbon\Carbon::now()->format('d.m.Y') }}')"--}}
                />
            </div>
            @if ($errors->has('startDay'))
                <span class="help-block">
                        <strong>{{ $errors->first('startDay') }}</strong>
                    </span>
            @endif


            <div class="form-group">
                <label for="endDay" class="form-label">Od:</label>
                <input class="form-control" type="date" value="{{ \Carbon\Carbon::now()->addDays(7)->toDateString() }}"
                       id="endDay" name="endDay"
                       min="{{ \Carbon\Carbon::now()->toDateString() }}"
                       {{--oninvalid="this.setCustomValidity('Dátum musí byť minimálne {{ \Carbon\Carbon::now()->format('d.m.Y') }}')"--}}
                />
            </div>
            @if ($errors->has('endDay'))
                <span class="help-block">
                        <strong>{{ $errors->first('endDay') }}</strong>
                    </span>
            @endif

            <button type="submit" class="btn btn-primary">Vyhľadaj termíny</button>
        </form>

    </div>

@endsection
