<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Seliga Peter">

        <title>
            @yield('my_title')
        </title>

        @yield('head_stuff')

    </head>

    <body>
        @yield('my_navbar')

        @if($flash = session('message'))
            <div id="flash-message" class="alert alert-success">
                {{ $flash }}
            </div>
        @endif
        @if($flash = session('warning'))
            <div id="flash-warning" class="alert alert-warning">
                {{ $flash }}
            </div>
        @endif
        @if($flash = session('danger'))
            <div id="flash-danger" class="alert alert-danger">
                {{ $flash }}
            </div>
        @endif
        @yield('content')

        @yield('my_footer')


        @include('scriptsImport')
        @yield('my_script_bottom')

    </body>
</html>
