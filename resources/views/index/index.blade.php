@extends('layout')


@section('my_title')
    Domov
@endsection


@section('head_stuff')
    @include('bootstrap')
@endsection


@section('my_navbar')
    @include('navbar')
@endsection


@section('content')
    <section class="jumbotron text-center">
		<div class="container">
			<h1 class="jumbotron-heading">Rezervačný systém podujatí detského oddelenia<br/>Krajskej knižnice v Žiline</h1>
			{{--<h2 class="jumbotron-heading">Priláste sa alebo si vytvorte<br/> nové konto</h2>--}}
			{{-- <p class="lead text-muted">
				Môžete sa prihlásiť ako
			</p> --}}
			<p>
                <a href="{{ url('/login') }}" class="btn btn-primary">Prihlásiť sa</a>
                <a href="{{ url('/register') }}" class="btn btn-secondary">Registrovať sa</a>
				{{-- <a href="/home" class="btn btn-primary">Užívateľ</a>
				<a href="/home" class="btn btn-secondary">Pracovník</a>
                <p class="lead text-muted">
                Nemáte konto?<br/>
                    <a href="/register" class="btn btn-default">Registrovať</a>
                </p> --}}
				<hr/>
				<p class="lead text-muted">
					alebo
				</p>
				<a href="{{ url('/events') }}" class="btn btn-default">Pokračovať bez prihlásenia</a>
			</p>
		</div>
	</section>
@endsection


@section('my_footer')
    <div class="text-muted pull-down">
        @include('footer')
    </div>
@endsection


@section('my_script_bottom')

@endsection
