@extends('layout')


@section('my_title')
    Kalendár
@endsection


@section('head_stuff')

    @include('bootstrap')

@endsection


@section('my_navbar')
    @include('navbar')
@endsection


@section('content')
    <div class="container-fluid mt-5 mb-5">


            {!! $calendar->calendar() !!}

        <div class="row mt-3">
            <div class="col">
                <ul>
                    <li>
                        <span class="text-info">Rozdielne farby orámovania vrchnej časti udalosti označujú miestnosť.</span>
                    </li>
                    <li>
                        <span class="text-info">Farba celej udalosti:
                            <ul>
                                <li>
                                    <b>červená</b> - udalosť ešte nebola potvrdená čakajte na potvrdenie
                                </li>
                                <li>
                                    <b>zelená</b> - udalosť bola potvrdená a je platná
                                </li>
                                <li>
                                    <b>modrá</b> - udalosť nepatrí vám
                                </li>
                            </ul>
                        </span>
                    </li>
                </ul>
            </div>
            {{--<div class="col">--}}
                {{--@foreach(\App\Place::all() as $place)--}}
                    {{--<label>{{ $place->name }}:</label>--}}
                    {{--<div class="d-inline-block colorBlock"></div> <br/>--}}
                {{--@endforeach--}}
            {{--</div>--}}

        </div>
    </div>
@endsection


@section('my_footer')

    @include('footer')
@endsection

@section('my_script_bottom')


    <style type="text/css">
        .moreBorder{
            border-top-width: 3px;
        }

        /*.colorBlock{*/
            /*width: 1.5em;*/
            /*height: 1.5em;*/
            /*background: red;*/
        /*}*/
    </style>

    {{--<script type="text/javascript">--}}

        {{--var id = '#calendar-' +  {!! @json_encode($calendar->getId()) !!};--}}
        {{--$(document).ready(--}}
            {{--function () {--}}

                {{--$(id).fullCalendar('changeView', 'agendaWeek');--}}

                {{--var view = $(id).fullCalendar('getView');--}}
        {{--});--}}

        {{--$('body').on('click', 'button.fc-prev-button', function() {--}}

            {{--var view = $(id).fullCalendar('getView');--}}
            {{--if (view.name === 'agendaWeek'){--}}
                {{----}}
            {{--}--}}
        {{--});--}}

        {{--$('body').on('click', 'button.fc-next-button', function() {--}}

        {{--});--}}


    {{--</script>--}}

@endsection
