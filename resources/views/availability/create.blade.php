@extends('events/layout')


@section('my_title')
    {{ $title }}
@endsection


@section('content')

    <div class="container col-md-8 jumbotron">

        <h1 class="jumbotron-heading">{{ $title }}</h1>
        {{--<span class="text-info">{{ \Carbon\Carbon::parse($availab->start_time)->format('H:i') }} - {{ \Carbon\Carbon::parse($availab->end_time)->format('H:i d.m.Y') }}</span>--}}
        <hr/>


        @if(isset($calendar) && empty($startTime))

            <span class="text-info">
                Vyberte dni, kedy chcete zamestnancovi {{$user->name}} zadať dostupnosť.<br/>
                Vo vybraté dni sa mu nastaví nižšie špecifikovaný začiatok a koniec.
            </span>
            <div class="container-fluid mt-5 mb-5">
                {!! $calendar->calendar() !!}
            </div>

        @endif


        <form method="POST" action="{{ url($action) }}" id="eventForm" enctype="multipart/form-data">
            {{ csrf_field() }}

            @if(isset($startTime))
                <div class="form-group">
                    <label for="date">Dátum:</label>
                    <input type="date" name="date" id="date" class="form-control"
                           value="{{ isset($startTime) ? $startTime->toDateString() : \Carbon\Carbon::tomorrow()->toDateString() }}"
                           required/>
                </div>
                @if ($errors->has('date'))
                    <span class="help-block">
                <strong>{{ $errors->first('date') }}</strong>
            </span>
                @endif
            @endif

            <div class="form-group">
                <label for="startTime">Začiatok:</label>
                <input type="time" name="startTime" id="startTime" class="form-control"
                       value="{{ isset($startTime) ? $startTime->format('H:i') : "" }}" required/>
            </div>
            @if ($errors->has('startTime'))
                <span class="help-block">
                <strong>{{ $errors->first('startTime') }}</strong>
            </span>
            @endif


            <div class="form-group">
                <label for="endTime">Koniec:</label>
                <input type="time" name="endTime" id="endTime" class="form-control"
                       value="{{ isset($endTime) ? $endTime->format('H:i') : (isset($startTime) ? $startTime->addHours(8)->format('H:i') : "") }}"
                       required/>
            </div>
            @if ($errors->has('endTime'))
                <span class="help-block">
                <strong>{{ $errors->first('endTime') }}</strong>
            </span>
            @endif

            @if(isset($name))
                <div class="form-group">
                    <label for="name">Názov:</label>
                    <input type="text" name="name" id="name" class="form-control" value="BLOK" required/>
                </div>
                @if ($errors->has('name'))
                    <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
                @endif
            @endif

            @if(isset($subject))
                <div class="form-group">
                    <label for="subject">Popis:</label>
                    <input type="text" name="subject" id="subject" class="form-control"
                           value="V tomto čase je zamestnanec neobsaditeľný." required/>
                </div>
                @if ($errors->has('subject'))
                    <span class="help-block">
                <strong>{{ $errors->first('subject') }}</strong>
            </span>
                @endif
            @endif


            <input name="userId" id="userId" value="{{ $user->id }}" type="hidden"/>
            <input name="availabs" id="availabs" value="" type="hidden"/>


            <button type="submit" class="btn btn-primary">Potvrdiť</button>

        </form>


    </div>

@endsection


@section('my_script_bottom')

    @if(isset($calendar) && empty($startTime))
        <script type="text/javascript">
            var id = {!! @json_encode('#calendar-' . $calendar->getId()) !!};
            var user = {{ $user->id }};

            function createEvent(date) {

                var events = $(id).fullCalendar('clientEvents', function (event) {
                    if (event.start.get('date') === date.get('date') && event.start.get('year') === date.get('year')) {
                        {{--// console.log(event);--}}
                            return true;
                    }
                    {{--// console.log(event);--}}
                        return false;
                });

                if (events.length >= 1) {
                    {{--// events[0]--}}
                    $(id).fullCalendar('removeEvents', events[0]._id);
                    {{--// console.log(events[0]._id);--}}
                }
                else {


                    $(id).fullCalendar('renderEvent', {
                        title: 'vybrané',
                        start: date,
                        allDay: true

                    }, true);
                }

                {{--
                // var url = ("/availability/delete" + delEvts);
                // console.log(url);
                // window.location.replace(url);
                --}}
            }

            $(document).ready(
                //#eventForm
                $('#eventForm').submit(function (e) {

                    var events = $(id).fullCalendar('clientEvents');

                    var dates = [];

                    events.forEach(function (event)
                    {
                        {{--  console.log(event.start);
                        console.log($( "input[name='_token']" ).val()); --}}
                        dates.push(event.start._d);

                    });

                    $("input[name='availabs']").val(JSON.stringify(dates));



                    {{--$.ajax({--}}
                    {{--type: "POST",--}}
                    {{--url: "{{ url('/availability/create') }}",--}}
                    {{--data: {--}}
                    {{--'_token': $("input[name='_token']" ).val(),--}}
                    {{--'availabs': dates,--}}
                    {{--'startTime': $("input[name='startTime']" ).val(),--}}
                    {{--'endTime': $("input[name='endTime']" ).val(),--}}
                    {{--'userId' : user--}}
                    {{--}--}}
                    {{--}).done(function (data) {--}}
                    {{--if (data == "Uspesne")--}}
                    {{--{--}}
                    {{--window.location.replace( "{{ url('/availability/show/?user=' . $user->id) }}" );--}}
                    {{--}--}}
                    {{--});--}}


                    {{--$.post("{{ url('/availability/create') }}",--}}
                    {{--{--}}
                    {{--'_token': $("input[name='_token']" ).val(),--}}
                    {{--'availabs': dates,--}}
                    {{--'startTime': $("input[name='startTime']" ).val(),--}}
                    {{--'endTime': $("input[name='endTime']" ).val(),--}}
                    {{--'userId' : user--}}
                    {{--}).done(function (data) {--}}
                    {{--if (data == "Uspesne")--}}
                    {{--{--}}
                    {{--window.location.replace( "{{ url('/availability/show/?user=') }}" + user);--}}
                    {{--}--}}
                    {{--});--}}


                })
            );


        </script>

    @endif
@endsection
