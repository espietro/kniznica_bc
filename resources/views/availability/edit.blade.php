@extends('events/layout')


@section('my_title')
    Upraviť Dostupnosť
@endsection


@section('content')

    <div class="container col-md-8 jumbotron">

        <a href="{{ url('/availability/' . $availab->id . '/delete') }}" class="btn btn-outline-danger float-right">Zmazať</a>
        <h1 class="jumbotron-heading">Upraviť Dostupnosť id:{{ $availab->id }}</h1>
        <span class="text-info">{{ \Carbon\Carbon::parse($availab->start_time)->format('H:i') }} - {{ \Carbon\Carbon::parse($availab->end_time)->format('H:i d.m.Y') }}</span>
        <hr/>
        <form method="POST" action="{{ url('/availability/' . $availab->id . '/update') }}" id="eventForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}

            <div class="form-group">
                <label for="startTime">Začiatok:</label>
                <input type="time" name="startTime" id="startTime" class="form-control" value="{{ \Carbon\Carbon::parse($availab->start_time)->toTimeString() }}" required/>
            </div>
            @if ($errors->has('startTime'))
                <span class="help-block">
                <strong>{{ $errors->first('startTime') }}</strong>
            </span>
            @endif


            <div class="form-group">
                <label for="endTime">Koniec:</label>
                <input type="time" name="endTime" id="endTime" class="form-control" value="{{ \Carbon\Carbon::parse($availab->end_time)->toTimeString() }}" required/>
            </div>
            @if ($errors->has('endTime'))
                <span class="help-block">
                <strong>{{ $errors->first('endTime') }}</strong>
            </span>
            @endif


            <button type="submit" class="btn btn-primary">Potvrdiť</button>

        </form>


    </div>

@endsection
