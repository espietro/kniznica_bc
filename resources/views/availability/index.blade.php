@extends('events/layout')


@section('my_title')
    Výber Zamestnanca
@endsection


@section('content')

    <div class="container col-md-8 jumbotron">
        <h1 class="jumbotron-heading">Vyberte zamestnanca</h1>
        <hr/>

        <form class="mb-5" method="GET" action="{{ url('/availability/show') }}" id="occurrenceForm" enctype="multipart/form-data">
            {{--{{ csrf_field() }}--}}

            <div class="form-group">
                <label for="user">Ktorý zamestnanec? <span class="text-muted">(meno -> email)</span></label>
                <select class="form-control" id="user" name="user">
                    @foreach($users as $user)
                        @if($user->isEmployee())
                            <option value="{{ $user->id }}">
                                {{ $user->name }} -> {{ $user->email }}
                            </option>
                        @endif
                    @endforeach
                    <option value="0">Všetci zamestnanci</option>
                </select>
            </div>
            @if ($errors->has('user'))
                <span class="help-block">
                    <strong>{{ $errors->first('user') }}</strong>
                </span>
            @endif

            <button type="submit" class="btn btn-primary">Zobraz dostupnosť</button>
        </form>

    </div>

@endsection
