@extends('events/layout')


@section('my_title')
    Dostupnosť
@endsection


@section('content')

    <div class="container-fluid mt-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-9">
                    @if(isset($user))
                        <h1 class="jumbotron-heading">Pracovný čas pre zamestnanca {{ $user->name }}</h1>
                    @endif

                    @if(auth()->user()->isEmployee() && isset($user) && empty($adminAccess))
                        <span class="text-info">Kliknutím na dostupnosť môžete vytvoriť blok kedy vás nebude mocť žiaden užívateľ rezervovať do Udalosti.</span>
                    @endif
                    @if(isset($user) && auth()->user()->isAdmin() && isset($adminAccess))
                        <span class="text-info">Kliknutím na miesto v kalendári môžete vytvoriť novú dostupnosť pre tohto zamestnanca.</span>
                        <h3> Zamestnanec: {{ $user->name }} <span class="text-muted">id:{{ $user->id }}</span></h3>
                    @endif
                </div>
                <div class="col-3">
                    @if(auth()->user()->isAdmin() && isset($adminAccess))
                        @if(isset($user))

                            <a href="{{ url('/availability/create/'. $user->id) }}" class="btn btn-outline-primary float-right"
                               title="Vytvoriť pre tohto zamestnanca novú dostupnosť.">Pridať dostupnosť</a>
                        @endif
                        <a title="Nenávratne odstráni vybraté dostupnosti."
                           class="btn btn-outline-danger float-right mr-2"
                           id="now">Zmazať</a>
                    @endif
                </div>

            </div>
        </div>
        <hr/>

        {!! $calendar->calendar() !!}

        <div class="row m-3">
            <div class="col">
                <ul>
                    <li><span class="text-info">Rozdielne farby vrchnej časti udalosti označujú miestnosť. (červená znamená BLOK)</span></li>
                    <li><span class="text-info">Iba Udalosti majú orámovanie inou farbou.</span></li>
                    <li><span class="text-info">Dostupnosti sú väčšie bloky v porovnaní s Udalosťami, tie sú menšie.</span></li>
                    <li><span class="text-info">Dostupnosti a Udalosti ktoré patria jednému zamestnancovi sú zobrazené rovnakou farbou. </span></li>
                </ul>
            </div>
        </div>
    </div>

@endsection

@section('my_footer')

    @include('footer')


@endsection

@section('my_script_bottom')

    <script type="text/javascript">
        var id = {!! @json_encode('#calendar-' . $calendar->getId()) !!};

        $(document).ready(
            $('#now').click(function () {

                var events = $(id).fullCalendar('clientEvents');
                // alert(id);
                var delEvts = '?';
                var i = 0;
                events.forEach(function (event) {
                    if (event.clicked) {
                        // console.log(event);
                        var and = i === 0 ? '' : '&';
                        delEvts += (and + i + '=' + event.id);
                        i++;
                    }

                });
                var url = ("{{ url('/availability/delete' ) }}" + delEvts);
                // console.log(url);
                window.location.replace(url);
            })
        );

    </script>

    <style type="text/css">
        .moreBorder{
            border-top-width: 3px;
        }
    </style>

@endsection
