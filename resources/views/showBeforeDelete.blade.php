@extends('events/layout')

@section('my_title')
    {{ $title }}

@endsection


@section('content')

    <div class="container col-md-8 jumbotron">
        <h1 class="jumbotron-heading">{{$title}}</h1>
        @if(isset($info))
            <span class="text-warning">POZOR!</span>
            <span class="text-info">{{ $info }}</span>
        @endif
        <hr/>
    <form method="POST" action="{{ $action }}">

        {{ csrf_field() }}
        <div class="form-group">
            <label for="{{ $label }}">{{ $question }}</label>
            <select class="form-control" id="{{ $label }}" name="{{ $label }}">
                @foreach($items as $item)
                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
        </div>

        <button type="submit" class="btn btn-primary delete-place">Zmazať</button>
    </form>
    </div>




@endsection


@section('my_script_bottom')
@if($label == 'places')


        <script type="text/javascript">

            $(document).ready(
                function () {
                    $(".delete-place").click( function (event) {
                        var answer = confirm("Naozaj chcete odstrániť vybranú miestnosť?" +
                            "\nOdstránená bude miestnosť, spolu so všetkými udalostami spojenými s touto miestnosťou." +
                            "\nTúto akciu nie je možné vrátiť späť.");

                        if (!answer)
                        {
                            event.preventDefault();
                        }
                    });

                });

        </script>


@endif
@endsection

