@extends('events/layout')


@section('my_title')
    Podujatia
@endsection


@section('my_navbar')
    @include('navbar')
@endsection


@section('my_content')    
    <div class="row">
        <div class="col-sm-1"></div>

        <div class="col-md-3">

            {{--<div class="mt-4">--}}
                {{--{!! $calendar->calendar() !!}--}}
            {{--</div>--}}


                <div class="mt-5">
                    <ul>

                        @if(\Auth::check() && \Auth::user()->isAdmin())
                            <span class="text-muted ">Administrátor</span>
                            <hr class="mt-0"/>
                            <li>
                                <h6>
                                    <a href="{{ url('/users/edit') }}">Pridať/Odobrať Užívateľovi Práva</a>
                                </h6>
                            </li>
                            <li>
                                <h6>
                                    <a href="{{ url('/availability/index') }}">Pridať/Odobrať Užívateľovi Dostupnosť</a>
                                </h6>
                            </li>

                            <li>
                                <h6>
                                    <a title="Miestnosť sa bude dať priradiť k novým alebo existujúcim podujatiam." href="{{ url('/places/create') }}">Vytvoriť Novú Miestnosť</a>
                                </h6>
                            </li>
                            <li>
                                <h6>
                                    <a title="Odstráni vybratú miestnosť." class="text-warning" href="{{ url('/places/index') }}">Zmazať Miestnosť</a>
                                </h6>
                            </li>
                            <h6 class="mb-4 spacer"></h6>
                        @endif

                        @if(\Auth::check() && \Auth::user()->isEmployee())
                            <span class="text-muted ">Zamestnanec</span>
                            <hr class="mt-0"/>
                            <li>
                                <h6>
                                    <a title="Zobrazí kalendár s dostupnosťou zamestnanca a zároveň udalosti ktoré v rámci jeho dostupnosti organizuje." href="{{ url('/users/availabs') }}">Zobraziť Moje Dostupnosti</a>
                                </h6>
                            </li>
                            <li>
                                <h6>
                                    <a title="Vytvoriť nové podujatie (trieda/prototyp), ktoré bude mocť byť použité v konkrétnej udalosti (inštancia)." href="{{ url('/events/create') }}">Vytvoriť Nové Podujatie</a>
                                </h6>
                            </li>
                            <li>
                                <h6>
                                    <a title="Cieľová skupina sa bude dať priradiť k novým a existujúcim podujatiam. Vďaka čomu budú zákazníci mocť vyhľadať tieto podujatia podľa cieľovej skupiny." href="{{ url('/tgroups/create') }}">Vytvoriť Novú Cieľovú Skupinu</a>
                                </h6>
                            </li>

                            <li>
                                <h6>
                                    <a title="Odstráni vybratú cieľovú skupinu." class="text-warning" href="{{ url('/tgroups/index') }}">Zmazať Cieľovú Skupinu</a>
                                </h6>
                            </li>

                            <li>
                                <h6>
                                    <a title="Zmení autora vybraným podujatiam, ktorých som autor, na vybraného zamestnanca." href="{{ url('/events/creator') }}">Zmeniť autora mojim podujatiam</a>
                                </h6>
                            </li>
                            <h6 class="mb-4 spacer"></h6>
                        @endif
                            <span class="text-muted mt-5">Užívateľ</span>
                            <hr class="mt-0"/>
                            <li>
                            <h6>
                                <a title="Vhodné vtedy, ak ste si už vybrali podujatie z ponuky na ktoré by ste sa chceli prihlásiť a chcete zistit kedy najbližšie ho bude možné zorganizovať." href="{{ url('/occurrences/selectEvent') }}">Chcem sa prihlásiť na vybrané podujatie</a>
                            </h6>
                        </li>
                        <li>
                            <h6>
                                <a title="Vhodné vtedy, ak poznáte cieľovú skupinu a dátum, kedy chcete prísť. Táto možnosť zobrazí podujatia vhodné pre zvolenú cieľovú skupinu v tento deň." href="{{ url('/occurrences/selectNoEvent') }}">Chcem sa prihlásiť na podujatie podľa cieľovej skupiny</a>
                            </h6>
                        </li>
                    </ul>
                </div>

        </div>

        <div class="col-md-7 p-0">
           @include('events/show')
        </div>
        
    </div>
@endsection

