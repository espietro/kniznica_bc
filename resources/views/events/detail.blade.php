@extends('events/layout')


@section('my_title')
    {{ $event->title }}
@endsection


@section('content')
    <div class="container">
        @if(\Auth::check() && \Auth::user()->isCreator($event->id))
            <a class="btn btn-outline-danger float-right" href="{{ url('/events/' . $event->id . '/delete') }}">Zmazať</a>
        @endif
        <h1 class="mt-5">{{ $event->title }}</h1>
        <hr/>

        @if(\Auth::check() && \Auth::user()->isAdmin())
            <b>ID:</b>&nbsp;{{ $event->id }}<br/>
        @endif

        <b>Trvanie:</b>&nbsp;{{ $event->duration }} minút<br/>

        @if(\Auth::check() && \Auth::user()->isEmployee())
            <b>Vytvoril\a:</b>&nbsp;{{ $event->creator->name }}<br/>

            <b>Moze/u organizovat:</b>&nbsp;
            <p>
                @foreach ($event->users as $user)
                    {{ $user->name }}<br/>
                @endforeach
            </p>

            <b>Mozne miestnosti:</b>&nbsp;
            <p>
                @foreach ($event->places as $place)
                    {{ $place->name }}
                @endforeach
            </p>

            <b>Kedy:</b>&nbsp;
            <p>{{ $event->created_at->diffForHumans() }}
                / {{ $event->created_at }}<br/></p>

            <b>Je viditelny:</b>
            <p>
                {{ $event->visible ? 'Ano' : 'Nie' }}
            </p>
        @endif

        <b>Popis:</b>&nbsp;<p>{{ $event->subject }}<br/></p>
        @if(sizeof($event->tgroups)==1)
            <b>Cielova skupina:</b>&nbsp;
        @else
            <b>Cielove skupiny:</b>&nbsp;
        @endif
        <ul>
            @foreach($event->tgroups as $tgroup)
                <li>{{ $tgroup->name }}</li>
            @endforeach
        </ul>
        <hr/>
    </div>
@endsection
