<h1 class="m-4">Podujatia v ponuke</h1>

@if(count($events))
    @foreach($events as $event)
        @if($event->visible || Auth::check() && (Auth::user()->isCreator($event->id) || auth()->user()->isAdmin()))
            <div class="row m-1">

                <div class="card col-12">
                    <div class="card-header p-1">
                        <a href="{{ url('/events/' . $event->id) }}">
                            <img class="mr-3" height="135" width="240" src="{{ asset('storage/tyzdenslovenskychkniznic.png') }}"
                                 alt="Foto: Krajska Kniznica V Ziline">
                        </a>
                        @if(Auth::check() && (\Auth::user()->isCreator($event->id) || \Auth::user()->isAdmin()))
                            <a href="{{ url('/events/' . $event->id . '/edit') }}"
                               class="float-right btn btn-outline-primary btn-sm">Upraviť</a>
                        @endif
                    </div>

                    <div class="card-body bg-light">
                        <h5>
                            <a href="{{ url('/events/' . $event->id) }}">
                                {{ $event->title }}
                            </a>
                        </h5>
                        <p>
                            {{ $event->subject }}
                        </p>
                    </div>
                    <div class="card-footer p-2">
                        @if(\Auth::check() && \Auth::user()->isEmployee())
                        <h6 class="float-left">{{ isset($event->creator->name) ? $event->creator->name : "Autor nezadaný" }}</h6>
                        <strong class="float-right">{{ $event->created_at->diffForHumans() }}</strong>
                        @endif
                    </div>
                </div>
            </div>
        @endif
    @endforeach
@else
    <p class="text-info text-center">V ponuke nie sú žiadne podujatia</p>
@endif
