@extends('events/layout')


@section('my_title')
    {{ $title }}
@endsection


@section('content')

    <div class="container col-md-8 jumbotron">


        <form method="POST" action="{{ url($action) }}" id="eventForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}


            <div class="form-group">
                <label for="events" class="form-label">Podujatia kde som autorom:</label>
                <select multiple id="events" name="events[]" class="form-control" required
                        {{--oninvalid="this.setCustomValidity('Prosím vyberte aspoň jednu cieľovú skupinu')"--}}
                >
                    @foreach($events as $event)
                        <option value="{{ $event->id }}">{{ $event->title }}</option>
                    @endforeach
                </select>
            </div>





            <div class="form-group">
                <label for="newCreator">Nový autor:<span class="text-muted">(meno -> email)</span></label>
                <select class="form-control" id="newCreator" name="newCreator" required>
                    @foreach($users as $user)
                        <option value="{{ $user->id }}">
                            {{ $user->name }} -> {{ $user->email }}
                        </option>
                    @endforeach
                </select>
            </div>
            @if ($errors->has('newCreator'))
                <span class="help-block">
                    <strong>{{ $errors->first('newCreator') }}</strong>
                </span>
            @endif

            <button type="submit" class="btn btn-primary">Potvrdiť</button>

        </form>


    </div>

@endsection

