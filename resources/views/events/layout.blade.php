@extends('layout')


@section('head_stuff')

    @include('bootstrap')
@endsection


@section('my_navbar')
    @include('navbar')
@endsection


@section('content')    
    @yield('my_content')
@endsection


@section('my_footer')
    <div class="m-2">
        @include('footer')
    </div>
@endsection

@section('my_script_bottom')


@endsection
