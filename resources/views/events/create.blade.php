@extends('events/layout')


@section('my_title')
    Vytvoriť Podujatie
@endsection


@section('content')

    <div class="container col-md-8 jumbotron">

        <h1 class="jumbotron-heading">Nové Podujatie</h1>
        <hr/>

        <form method="POST" action="{{ url('/events/create') }}" id="eventForm" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="title">Názov:</label>
                <input type="text" name="title" id="title" class="form-control" required/>
            </div>
            @if ($errors->has('title'))
                <span class="help-block">
                    <strong>{{ $errors->first('title') }}</strong>
                </span>
            @endif


            <div class="form-group">
                <label for="subject">Téma/Popis:</label>
                <textarea id="subject" name="subject" class="form-control" rows="4" cols="50"></textarea>
            </div>
            @if ($errors->has('subject'))
                <span class="help-block">
                    <strong>{{ $errors->first('subject') }}</strong>
                </span>
            @endif


            <div class="form-group">
                <label for="duration">Trvanie:</label>
                <input type="number" name="duration" id="duration" class="form-control" min="10" step="5" required/>
            </div>
            @if ($errors->has('duration'))
                <span class="help-block">
                    <strong>{{ $errors->first('duration') }}</strong>
                </span>
            @endif


            <div class="form-group">
                <label for="tgroup">Cieľová skupina:</label>
                @foreach($tgroups as $tgroup)
                    <label>
                        <input type="checkbox" id="tgroup{{ $tgroup->id }}" name="tgroup[]" value="{{ $tgroup->id }}">
                        &nbsp;{{ $tgroup->name }}
                    </label>
                @endforeach
                <a href="{{ url('/tgroups/create') }}">Nová</a>
                </select>
            </div>
            @if ($errors->has('tgroup'))
                <span class="help-block">
                <strong>{{ $errors->first('tgroup') }}</strong>
            </span>
            @endif


            <div class="form-group">
                <label for="place">Miestnosť(i):</label>
                @foreach($places as $place)
                    @if($place->id > 0)
                        <label>
                            <input type="checkbox" id="place{{ $place->id }}" name="place[]" value="{{ $place->id }}">
                            &nbsp;{{ $place->name }}
                        </label>
                    @endif
                @endforeach
                <a href="{{ url('/places/create') }}">Nová</a>
                </select>
            </div>
            @if ($errors->has('place'))
                <span class="help-block">
                    <strong>{{ $errors->first('place') }}</strong>
                </span>
            @endif


            <div class="form-group">
                <label for="employee">Organiztor(i):</label>
                @foreach($employees as $employee)
                    @if($employee->id != \Auth::id())
                        <label>
                            <input type="checkbox" id="employee{{ $employee->id }}" name="employee[]"
                                   value="{{ $employee->id }}">
                            &nbsp;{{ $employee->name }}
                        </label>
                    @endif
                @endforeach
                <a href="{{ url('/employees/create') }}">Nový</a>
                </select>
            </div>
            @if ($errors->has('employee'))
                <span class="help-block">
                    <strong>{{ $errors->first('employee') }}</strong>
                </span>
            @endif


            <div class="form-group">
                <label for="visible">Neviditeľný pre užívateľa:</label>
                <input type="checkbox" id="visible" name="visible">
            </div>
            @if ($errors->has('visible'))
                <span class="help-block"><strong>{{ $errors->first('visible') }}</strong></span>
            @endif


            <div>
                <button type="submit" class="btn btn-primary">Potvrdiť</button>
            </div>
        </form>
    </div>

@endsection
