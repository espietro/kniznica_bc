@extends('events/layout')

@section('my_title')
    Upraviť práva užívateľov
@endsection


@section('content')

    <div class="container col-md-10">

        <h1 class="jumbotron-heading mt-5">Upraviť práva užívateľov</h1>
        <h6 class="text-info">Možné akcie:
            <ul>
                <li>Vyberte ktorým užívateľom chcete nastaviť vybranú rolu (je možné hromadné pridelenie/odobratie role).</li>
                <li>Odstránte konkrétny účet.</li>
                <li>Nastavte konkrétneho zamestnanca ako Kontaktnú osobu.
                    <ul>
                        <li class="text-muted">Kontaktná osoba je vyznačená <span class="bg-warning text-primary">Oranžovou farbou</span>.</li>
                    </ul>
                </li>
            </ul>
        </h6>
        <hr/>
        <form method="POST" action="{{ url('/users/update') }}">
            {{ csrf_field() }}


            <div class="container-fluid mt-4">
                <div class="form-inline">
                    <span class="text-primary col-md-6 ">Vyberte rolu, ktorú chcete priradiť vybraným používateľom:</span><br/>
                    <span class="col-3">
                        <select id="role_select" name="roles">
                            <option value="" selected disabled hidden>Vybrať</option>
                            @foreach($roles as $role)
                                <option value="{{ $role->id }}">{{ $role->name }}</option>
                            @endforeach
                            <option value="0">Odobrať aktuálne práva</option>
                        </select>
                    </span>
                    <span class="col-md-3">
                        <button title="Nastaví vybraným užívateľom zvolené práva. Ostatné sa im odoberú." type="submit"
                                class=" btn btn-primary roles_submit_btn">Zmeniť práva
                        </button>
                    </span>
                </div>


            </div>





            <table class="table table-bordered mt-2">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Meno</th>
                    <th>Mail</th>
                    <th>Telefón</th>
                    <th>Škola</th>
                    <th>Aktuálna rola</th>
                    <th>Vybrať</th>
                    <th>Akcia</th>
                </tr>
                </thead>
                <tbody>

                @foreach($users as $user)
                    <tr class=@if($user->id == $contactId) "bg-warning" @endif>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->phone }}</td>
                        <td>{{ $user->institution }}</td>
                        <td style="white-space: nowrap; width: 1px;">
                            @foreach($user->roles as $role)
                                {{ $role->name . ',' }}
                            @endforeach
                        </td>
                        <td style="white-space: nowrap; width: 1px;"><input type="checkbox" name="users[]" id="users[]" value="{{ $user->id }}"/></td>
                        <td style="white-space: nowrap; width: 1px;">
                            <div class="btn-group" >

                                <a title="Užívateľ bude nenávratne zmazaný z databázy." href="{{ url('/users/' . $user->id . '/delete') }}"
                                   class="btn btn-sm btn-outline-danger delete">Zmazať</a>
                                @if($user->isEmployee() && $user->id != $contactId)
                                    <a title="Zamestnanec sa stane kontaktnou osobou, tým sa stane prostredníkom medzi používateľom a zamestnancom. Po rezervácii podujatia používateľom je upozornený email-om, následne môže pomocou systému prideliť organizátora požadovanému podujatiu podľa dostupnosti zamestnancov."
                                       class="btn btn-sm btn-outline-warning " href="{{ url('/users/' . $user->id . '/contact') }}">Kontaktná osoba</a>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>




        </form>

    </div>

@endsection


@section('my_script_bottom')
    <script type="text/javascript" src="{{ URL::asset('js/confirm.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(
            function () {
                $(".roles_submit_btn").click( function (event) {

                    if ($("#role_select option:selected").text() === 'Vybrať' && !$("#contact_person").is(":checked"))
                    {
                        event.preventDefault();
                        alert("Nevybrali ste žiadnu rolu, ktorú chcete priradiť vybraným používateľom.");
                    }
                });
            });
    </script>
@endsection